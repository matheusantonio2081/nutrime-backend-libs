# Vizir Banking Libs

## Content

These are the available packages published by this repository:

- [@vb-libs/entity-builder](packages/entity-builder)
- [@vb-libs/secret-manager](packages/secret-manager)
- [@vb-libs/eslint-config-base](packages/eslint-config-base)
- [@vb-libs/eslint-config-node](packages/eslint-config-node)
- [@vb-libs/logger-winston-transporter](packages/logger-winston-transporter)
- [@vb-libs/logger](packages/logger)
- [@vb-libs/serverless-dotenv](packages/serverless-dotenv)
- [@vb-libs/strict-monapt](packages/strict-monapt)
- [@vb-libs/tsconfig-lib](packages/tsconfig-lib)
- [@vb-libs/tsconfig-node](packages/tsconfig-node)
- [@vb-libs/uuid](packages/uuid)
- [@vb-libs/http-client](packages/http-client)
- [@vb-libs/navigation](packages/navigation)
- [@vb-libs/navigation-react-dom](packages/navigation-react-dom)
- [@vb-libs/navigation-react-native](packages/navigation-react-native)
- [@vb-libs/hooks](packages/hooks)
- [@vb-libs/password-policy](packages/password-policy)
- [@vb-libs/database-password](packages/database-password)
- [@vb-libs/routing-controllers-middlewares](packages/routing-controllers-middlewares)

Check the README.md of each package individually for installation and
configuration instructions.

### Installing a package in the project

Use the individual instructions for each package.

## Contributing

### The basics

**First**, this repository hosts multiple packages, and uses
[yarn's workspaces](https://classic.yarnpkg.com/en/docs/workspaces/) feature
to configure this repository as a monorepo.

Each package must be placed inside the `packages` directory and yarn should
detect it automatically.

Don't run `yarn install` directly on a package directory. With workspaces
this command should be executed with this project's root `package.json`.

**Second**, any command/script must be working using this project's
docker-compose installation.

There is [Makefile](Makefile) configured to help running docker-compose's
tedious commands like:

```
make test-development
> docker-compose -f docker-compose-development.yml run --rm app ./scripts/run-all-tests.sh
```

**Third**, this repository has an integrated [workflow](.github/workflows) to
run the `make install-<environment> lint-<environment> test--<environment>` command, which will run the `lint` and
`test` script on each package when a new pull request is opened. Where `<environment>` can be `development` or
`production`.

Each package can customize what the `lint` and `test` scripts should do.

### About package.json

There are some rules to follow on the `package.json` configuration.

**First**, the package `name` must have the `@vb-libs` scope.

**Second**, with yarn's workspaces feature the `install` script should be
executed on this project's root `package.json`.

**Third**, with yarn's workspaces all dependencies will be hoisted on a
`node_modules` located on this project's root directory.

### Publishing new versions

To publish a new version, bump the package `version`, and run
`make patch-versions` to update the new version on dependencies from this repository.

Open a Pull Request with the modifications targeting the `master` branch.

Once the modifications are checked by the workflow and has at least one approval from
another person, you can merge it. Don't forget to clean unused branches.
