#!/bin/bash

packages=$(find packages -maxdepth 2 -type f -name package.json)

for package in $packages; do
  name=$(jq -r '.name' < "$package");
  version=$(jq -r '.version' < "$package");

  echo "$name@$version"
done;
