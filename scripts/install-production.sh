#!/bin/bash

set -euo pipefail

GITHUB_TOKEN=$1

git config --global url."https://token:$GITHUB_TOKEN@github.com/".insteadOf git@github.com:

yarn install

