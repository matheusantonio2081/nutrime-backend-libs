#!/bin/bash

set -euo pipefail

move_build_package_to_root() {
  local package_dir
  local production_build_dir_name

  package_dir=$1
  production_build_dir_name=production-build-$(cut -d / -f 2 <<< "$package_dir")

  find . -type f -not \( -path './.git/*' -or -path './builds/*' \) -delete
  find . -empty -type d -delete

  mv builds/"$production_build_dir_name"/* .

  rm -rf builds
  rm -rf node_modules
  rm -rf packages
}

generate_tag() {
  local package_dir
  local tagName
  local remoteTagName

  package_dir=$1
  echo "generate local tagname"
  tagName=$package_dir/$(jq -r '.version' "$package_dir"/package.json)

  echo "generate remote tagname"
  remoteTagName=$(git ls-remote origin --refs --tags origin "$tagName" | cut -f 2 | cut -d/ -f 3-5)

  echo "$tagName"
  echo "$remoteTagName"

  # Check if tag exists
  if [ "$tagName" == "$remoteTagName" ]; then
      echo "Skip tag creation. Tag ${tagName} already exist"
    else
      # Create and push tag
      git checkout -b temporary-branch
      move_build_package_to_root "$package_dir"
      git add .
      git commit -m "Create build tag ${tagName}"
      git tag "$tagName"
      git push origin "$tagName"

      # Remove temporary branch
      git checkout build-branch
      git branch -D temporary-branch
  fi
}

# START HERE

git config user.name gitlab-fretebras
git config user.email gitlab-fretebras@gitlab.com

CURRENT_BRANCH=$(git branch --show-current)
echo "$CURRENT_BRANCH"

# Create a intermediary commit to allow create tags for each package with only the package build files
git checkout -b build-branch
git add .
git commit -m "Added builds"

# find packages
packages=$(find packages -maxdepth 2 -type f -name package.json)

for package in $packages; do
  name="$(jq -r '.name' < "$package")"
  package_dir=$(dirname "$package")

  if [ -f "$package_dir/scripts/build-production.sh" ]; then
    printf '\n> %s\n' "$name"
    generate_tag "$package_dir"
  fi
done

git checkout "$CURRENT_BRANCH"
git branch -D build-branch