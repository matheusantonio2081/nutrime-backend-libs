#!/bin/bash

set -euo pipefail

make docker-build-development
make install-development
make create-production-builds
make lint-development
make test-development
make generate-tag-local

sudo chown -R "$(id -u):$(id -g)" .