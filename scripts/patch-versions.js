#!/usr/bin/env node
const packageScope = '@vb-libs';
const path = require('path');
const fs = require('fs');
const childProcess = require('child_process');

const packages = childProcess
  .execSync('find packages -maxdepth 2 -type f -name package.json  | tr "\n" ","', { cwd: process.cwd() })
  .toString()
  .split(',')
  .filter((x) => x !== '')
  .map((x) => path.join(process.cwd(), x))
  .map((packagePath) => {
    const data = require(packagePath);
    const name = data.name;
    const version = data.version;

    return {
      path: packagePath,
      name,
      version,
      data
    };
  });

for (const package of packages) {
  const replaceVersions = (dependencies) => {
    Object.keys(dependencies || []).forEach((dependency) => {
      if (!dependency.startsWith(packageScope)) return;
      if (!packages.some((p) => p.name === dependency)) return;
      const packageVersion = packages.find((p) => p.name === dependency).version;

      const dependencySplitted = dependencies[dependency].split("/");
      const dependencyVersion = dependencySplitted[dependencySplitted.length - 1];

      if (dependencyVersion === packageVersion) return;

      dependencySplitted.splice(-1, 1, packageVersion);
      const updatedDependency = dependencySplitted.join("/");

      console.log(`Updating ${dependency} ${dependencies[dependency]} to ${updatedDependency}`);
      dependencies[dependency] = updatedDependency;
    });
  };

  console.log(`\n> ${package.name}`)

  replaceVersions(package.data.dependencies);
  replaceVersions(package.data.devDependencies);

  fs.writeFileSync(package.path, `${JSON.stringify(package.data, null, 2)}\n`);
  console.log("Done.")
}
