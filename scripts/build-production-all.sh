#!/bin/bash

set -euo pipefail

packages=$(find packages -maxdepth 2 -type f -name package.json)

rm -rf builds
mkdir builds

# Create build files for each package and move to builds folder at root
for package in $packages; do
  name="$(jq -r '.name' < "$package")"
  package_dir=$(dirname "$package")
  if [ -f "$package_dir/scripts/build-production.sh" ]; then
    printf '\n> %s\n' "$name"
    production_build_dir_name=production-build-$(cut -d / -f 2 <<< "$package_dir")
    sh -c "cd $package_dir && ./scripts/build-production.sh $production_build_dir_name && mv $production_build_dir_name ../../builds"
  fi
done
