#!/bin/bash

set -euo pipefail

mkdir -p ~/.ssh
ssh-keyscan -H github.com >> ~/.ssh/known_hosts
