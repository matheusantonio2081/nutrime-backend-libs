#!/bin/bash

set -euo pipefail

packages=$(find packages -maxdepth 2 -type f -name package.json)

for package in $packages; do
  name="$(jq -r '.name' < "$package")"
  if [ "$(jq '.scripts.compile' < "$package")" != 'null' ]; then
    printf '\n> %s\n' "$name"
    sh -c "cd $(dirname "$package") && yarn compile"
  fi
done
