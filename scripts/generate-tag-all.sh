#!/bin/bash

set -euo pipefail

move_build_package_to_root() {
  local package_dir
  local production_build_dir_name

  package_dir=$1
  production_build_dir_name=production-build-$(cut -d / -f 2 <<< "$package_dir")

  find . -type f -not \( -path './.git/*' -or -path './builds/*' \) -delete
  find . -empty -type d -delete

  mv builds/"$production_build_dir_name"/* .

  rm -rf builds
  rm -rf node_modules
  rm -rf packages
}

generate_tag() {
  local package_dir
  local tagName

  package_dir=$1
  tagName=$package_dir/$(jq -r '.version' "$package_dir"/package.json)

  # Check if tag exists
  if [ "$(git tag -l "$tagName")" ]; then
      echo "Skip tag creation. Tag ${tagName} already exist"
    else
      # Create and push tag
      git checkout -b temporary-branch
      move_build_package_to_root "$package_dir"
      git add .
      git commit -m "Create build tag ${tagName}"
      git tag "$tagName"
      git push authenticatedGithubUrl "$tagName"

      # Remove temporary branch
      git checkout build-branch
      git branch -D temporary-branch
  fi
}

# START HERE

# Configure git
# GITHUB_TOKEN=$1
git remote add authenticatedGithubUrl "https://gitlab.fretebras.com.br/fretepago/banking/banking-libs.git"
git config user.name gitlab-fretebras
git config user.email gitlab-fretebras@gitlab.com

# Create a intermediary commit to allow create tags for each package with only the package build files
git checkout -b build-branch
git add .
git commit -m "Added builds"

# find packages
packages=$(find packages -maxdepth 2 -type f -name package.json)

for package in $packages; do
  name="$(jq -r '.name' < "$package")"
  package_dir=$(dirname "$package")

  if [ -f "$package_dir/scripts/build-production.sh" ]; then
    printf '\n> %s\n' "$name"
    generate_tag "$package_dir"
  fi
done
