#!/bin/bash

set -euo pipefail

packages=$(find packages -maxdepth 2 -type f -name package.json)

for package in $packages; do
  package_dir=$(dirname "$package")
  name="$(jq -r '.name' < "$package")"
  version="$(jq -r '.version' < "$package")"

  printf '\n> %s\n' "$name@$version"

  published_dist=$(yarn info --json "$name@$version" | jq .data.dist)

  if [ "$published_dist" == '' ] || [ "$published_dist" == 'null' ]; then
    if [ -f "$package_dir/scripts/publish.sh" ]; then
      sh -c "cd $package_dir && ./scripts/publish.sh"
    else
      sh -c "cd $package_dir && npm publish --unsafe-perm"
    fi
  else
    echo "Version $version is already published"
  fi;
done
