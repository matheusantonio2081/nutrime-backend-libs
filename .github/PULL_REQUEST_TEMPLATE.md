## Checklist

- [ ] Run `make lint` to lint the source code
- [ ] Run `make test` to run tests
- [ ] Run `make compile` to compile all source code
- [ ] Bumped the package to a new version and run `make patch-versions`
- [ ] Run `make install` to check if dependencies can be resolved
