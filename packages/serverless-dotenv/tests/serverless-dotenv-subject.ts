import { SinonSpy, spy } from "sinon";
import proxyquire from "proxyquire";

import { ServerlessDotenv } from "../lib/serverless-dotenv";

export class ServerlessDotenvSubject {
  public log: SinonSpy = spy();

  private definitions!: Record<string, string | undefined>;

  public createPlugin(writeSpy: SinonSpy = spy()): ServerlessDotenv {
    const ServerlessDotenvImport = proxyquire("../lib/serverless-dotenv.js", {
      fs: {
        createWriteStream: () => ({
          write: writeSpy,
        }),
      },
    });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-member-access
    return new ServerlessDotenvImport.ServerlessDotenv(
      {
        cli: {
          log: this.log,
        },
        service: {
          custom: {
            dotenv: this.definitions,
          },
        },
        pluginManager: {
          run: spy(),
        },
      },
      {},
    );
  }

  public withLog(log: SinonSpy): ServerlessDotenvSubject {
    this.log = log;

    return this;
  }

  public withDefinitions(definitions: Record<string, string | undefined>): ServerlessDotenvSubject {
    this.definitions = definitions;

    return this;
  }
}
