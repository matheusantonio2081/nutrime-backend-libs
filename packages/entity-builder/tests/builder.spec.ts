import faker from "faker";
import { expect } from "chai";

import { TestBuilder } from "./test-builder";
import { TestEntity } from "./test-entity";

describe("Builder", () => {
  it("should build an entity applying modifiers", () => {
    const builder = new TestBuilder();
    const modifiedValue = faker.random.word();
    const key: keyof TestEntity = faker.random.arrayElement(["hello", "world"]);
    const entity = builder.with(key, modifiedValue).build();

    expect(entity[key]).to.equal(modifiedValue);
  });

  it("should build multiple entities", () => {
    const builder = new TestBuilder();
    const modifiedValue = faker.random.word();
    const entities = builder.with("world", modifiedValue).buildMany();

    expect(entities.length).to.be.greaterThan(0);
    entities.forEach((entity) => {
      expect(entity.world).to.equal(modifiedValue);
    });
  });

  it("should build multiple entities by the desired quantity", () => {
    const builder = new TestBuilder();
    const modifiedValue = faker.random.word();
    const quantity = faker.datatype.number({ min: 2, max: 6 });
    const entities = builder.with("world", modifiedValue).buildMany(quantity);

    expect(entities.length).to.equal(quantity);
  });
});
