import faker from "faker";

import { Builder } from "../lib/builder";
import { TestEntity } from "./test-entity";

export class TestBuilder extends Builder<TestEntity, TestBuilder> {
  public constructor() {
    super(TestBuilder);
  }

  protected buildDefault(): TestEntity {
    return new TestEntity(
      faker.random.word(),
      faker.random.word(),
    );
  }
}
