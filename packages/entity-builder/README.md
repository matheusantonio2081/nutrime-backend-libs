# Entities Builder

Helper class to build entities for test purposes.

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/entity-builder/<version>
```

## Using

Create a class that implements the `Builder`

```typescript
import { Builder } from "@nu-libs/entity-builder";

class MyEntity {
  public constructor(public name: string) {}
}

class MyEntityBuilder extends Builder<MyEntity, MyEntityBuilder> {
  public constructor() {
    super(MyEntityBuilder);
  }

  protected buildDefault(): MyEntity {
    return new MyEntity("Foo");
  }
}

const builder = new MyEntityBuilder().with("name", "Bar");

const myEntity = builder.build();
// MyEntity { name: "Bar"}

const myEntities = builder.buildMany();
// [MyEntity { name: "Bar"}, MyEntity { name: "Bar" }, ...]
```
