# Strict monapt

This library is a thin wrapper on [monapt](https://github.com/jklmli/monapt).

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/strict-monapt/<version>
```

## Using

The "strict" monapt implement extra type-guarding methods on monapt's `Option`
so typescript can detect unwanted errors on compile time.

For example:

```typescript
import { Option } from "@nu-libs/strict-monapt";

const example = Option("example");

// This will cause a compile error
example.get();

// After checking the existence of a value
if (example.isDefined()) {
  // The method .get should be available to retrieve the "Example"
  example.get();
}
```
