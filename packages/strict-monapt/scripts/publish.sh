#!/bin/bash

set -e

yarn compile

rm -rf dist/tests
cp package.json dist
cp README.md dist

cd dist && npm publish
