import faker from "faker";
import { assert } from "chai";

import { None, Option } from "../../index";

let some: Option<string>;

describe("Some", function () {
  beforeEach(function () {
    some = Option("hello");
  });

  it(".isDefined", function () {
    assert.isTrue(some.isDefined());
  });

  it(".isEmpty", function () {
    assert.isFalse(some.isEmpty());
  });

  it(".equals", function () {
    const randomValue = faker.datatype.number();

    assert.isTrue(some.equals(Option("hello")));
    assert.isFalse(some.equals(Option(randomValue)));
    assert.isFalse(some.equals(None()));
  });

  it(".filter", function () {
    assert.deepEqual(some.filter((v: string) => v === "hello"), some);
    assert.deepEqual(some.filter((v: string) => v === "world"), None());
  });

  it(".filterNot", function () {
    assert.deepEqual(some.filterNot((v: string) => v === "hello"), None());
    assert.deepEqual(some.filterNot((v: string) => v === "world"), some);
  });

  it(".flatMap", function () {
    assert.deepEqual(some.flatMap(() => Option("world")), Option("world"));
  });

  it(".foreach", function () {
    let executions = 0;

    some.foreach(() => {
      executions += 1;
    });

    assert.equal(executions, 1);
  });

  it(".get", function () {
    if (some.isEmpty()) {
      assert.fail();
      return;
    }
    assert.equal(some.get(), "hello");
  });

  it(".getOrElse", function () {
    assert.equal(some.getOrElse(() => "world"), "hello");
  });

  it(".map", function () {
    assert.deepEqual(some.map((v: string) => `${v} world`), Option("hello world"));
  });

  it(".match", function () {
    assert.equal(some.match({
      some: (v: string): string => v,
      none: (): string => "world",
    }), "hello");
  });

  it(".orElse", function () {
    assert.deepEqual(some.orElse(() => Option("world")), Option("hello"));
  });

  it(".getOrThrow", function () {
    assert.equal(some.getOrThrow(), "hello");
  });
});
