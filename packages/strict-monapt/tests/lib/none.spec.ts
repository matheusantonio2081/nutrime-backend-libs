import faker from "faker";
import { assert } from "chai";

import { None, Option } from "../../index";

let none: Option<string>;

describe("None", function () {
  beforeEach(function () {
    none = Option();
  });

  it(".isDefined", function () {
    assert.isFalse(none.isDefined());
  });

  it(".isEmpty", function () {
    assert.isTrue(none.isEmpty());
  });

  it(".equals", function () {
    const randomValue = faker.datatype.number();

    assert.isTrue(none.equals(Option()));
    assert.isFalse(none.equals(Option(randomValue)));
    assert.isTrue(none.equals(None()));
  });

  it(".filter", function () {
    assert.deepEqual(none.filter(() => true), None());
    assert.deepEqual(none.filter(() => false), None());
  });

  it(".filterNot", function () {
    assert.deepEqual(none.filter(() => true), None());
    assert.deepEqual(none.filter(() => false), None());
  });

  it(".flatMap", function () {
    assert.deepEqual(none.flatMap(() => Option("world")), None());
  });

  it(".foreach", function () {
    let executions = 0;

    none.foreach(() => {
      executions += 1;
    });

    assert.equal(executions, 0);
  });

  it(".getOrElse", function () {
    assert.equal(none.getOrElse(() => "world"), "world");
  });

  it(".map", function () {
    assert.deepEqual(none.map((v: string) => `${v} world`), None());
  });

  it(".match", function () {
    assert.equal(none.match({
      some: (val: string): string => val,
      none: (): string => "world",
    }), "world");
  });

  it(".orElse", function () {
    assert.deepEqual(none.orElse(() => Option("world")), Option("world"));
  });

  it(".getOrThrow", function () {
    assert.throws(() => none.getOrThrow());
  });
});
