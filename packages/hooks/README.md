## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/hooks/<version>
```

## Using

### Routes Manager

Routes manager accept routes that implements [`AppRoute`](https://github.com/Vizir/banking-libs/blob/master/packages/navigation/README.md#app-route) interface.

```typescript
import { AppRoute, AppRouteConfigs } from "@nu-libs/navigation";
import { RoutesManager } from "@nu-libs/hooks";

class BankingAppRoute implements AppRoute {
  public constructor(
    public component: ReactElement,
    public configs: AppRouteConfigs
  ) {}

  public getConfig(): AppRouteConfigs {
    return this.configs;
  }

  public toReact(): ReactElement {
    return this.component;
  }
}

RoutesManager.addSection(
  "section",
  {
    someRoute: "/some-route",
  },
  (baseRoutes: AppRoute[]): AppRoute[] => {
    const newRoutes = [
      new BankingAppRoute(<SomeComponent />, {
        path: "/some-route",
        screenName: "some-route",
        isSensible: false,
        isPrivate: true,
      }),
    ];

    return [...baseRoutes, ...newRoutes];
  },
  0
);
```

### Translations Manager

Adding variables and generating translations

```typescript
import { TranslationsManager } from "@nu-libs/hooks";

TranslationsManager.addVariables({
  someEnv: "xxx",
});

TranslationsManager.add("en", "home", {
  someKey: "hello",
});

const generatedTranslations = TranslationsManager.generateTranslations("en");
```

## Icon and Image Manager

```typescript
import { IconManager, ImageManager } from "@nu-libs/hooks";

ImageManager.add("image-name", "image");
ImageManager.get("icon");

IconManager.add("icon", "icon-class");
IconManager.get("icon");
```

### Menu Manager

```typescript
import { MenuManager } from "@nu-libs/hooks";

const menuItems = [
  {
    label: "label",
    icon: "icon",
    order: 0,
    screenName: "screenName",
  },
];

// Adding new item
MenuManager.addMenuItem([menuItem]);

// Removing item
MenuManager.removeMenuItem("screenName");
```

### Redux Manager

To be implemented
