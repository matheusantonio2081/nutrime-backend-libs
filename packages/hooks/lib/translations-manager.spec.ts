import { assert, expect } from "chai";

import { TranslationsManager } from "./translations-manager";

const section = "home";

const translation = {
  someKey: "hello",
};

describe("TranslationsManager", function () {
  describe(".addVariables", function () {
    it("Should be able to add variable", function () {
      const variables = {
        someEnv: "lorem",
      };

      TranslationsManager.addVariables(variables);

      assert.deepStrictEqual(variables, TranslationsManager.getVariables());
    });
  });

  describe(".add", function () {
    it("Should be able to add translation", function () {
      TranslationsManager.add("en", section, translation);

      const generatedTranslations = TranslationsManager.generateTranslations(
        "en"
      );

      assert.deepStrictEqual(
        {
          [section]: translation,
        },
        generatedTranslations
      );
    });

    it("Should return empty object when translation key not found", function () {
      const generatedTranslations = TranslationsManager.generateTranslations(
        "pt-BR"
      );

      assert.deepStrictEqual({}, generatedTranslations);
    });

    it("Should be able to deep merge translations", function () {
      TranslationsManager.add("en", section, {
        ...translation,
        customDeep: {
          title: "title",
          subtitle: "subtitle",
        },
      });

      TranslationsManager.add("en", section, {
        customDeep: {
          subtitle: "subtitle modified",
        },
      });

      const generatedTranslations = TranslationsManager.generateTranslations(
        "en"
      );

      expect(generatedTranslations).to.deep.equal({
        [section]: {
          ...translation,
          customDeep: {
            title: "title",
            subtitle: "subtitle modified",
          },
        },
      });
    });
  });
});
