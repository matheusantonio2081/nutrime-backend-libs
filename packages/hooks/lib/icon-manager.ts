type IconClass = any;

export class IconManager {
  private static componentClasses: Record<string, IconClass> = {};

  public static add(name: string, componentClass: IconClass): void {
    IconManager.componentClasses[name] = componentClass;
  }

  public static get(name: string): IconClass {
    if (!IconManager.componentClasses[name]) {
      throw new Error(`Icon ${name} not found`);
    }

    return IconManager.componentClasses[name];
  }
}
