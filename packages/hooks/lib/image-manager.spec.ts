import { assert } from "chai";

import { ImageManager } from "./image-manager";

describe("ImageManager", function () {
  describe(".add", function () {
    before(() => {
      ImageManager.add("image-name", "image");
    });

    it("Should be able to add image", function () {
      assert.equal("image", ImageManager.get("image-name"));
    });

    it("Should throw when image is not found", function () {
      assert.throws(() => ImageManager.get("missing-image"));
    });
  });
});
