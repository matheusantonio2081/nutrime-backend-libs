export type MenuItem = {
  label: string;
  icon?: string;
  order: number;
  screenName: string;
};

export class MenuManager {
  private static menuItems: MenuItem[] = [];

  public static addMenuItems(menuItems: MenuItem[]): void {
    this.menuItems = [...this.menuItems, ...menuItems];
  }

  public static removeMenuItem(screenName: string): void {
    this.menuItems = this.menuItems.filter(
      (menuItem) => menuItem.screenName !== screenName,
    );
  }

  public static getMenuItems(): MenuItem[] {
    return this.menuItems.sort(
      (itemOne: MenuItem, itemTwo: MenuItem) => itemOne.order - itemTwo.order,
    );
  }

  public static reset(): void {
    this.menuItems = [];
  }
}
