import { ReactElement } from "react";
import flatten from "lodash/flatten";
import compact from "lodash/compact";
import isEmpty from "lodash/isEmpty";
import merge from "lodash/merge";

export interface AppRouteConfigs {
  isPrivate: boolean;
  isSensible: boolean;
  path: string;
  screenName: string;
}

export interface AppRoute {
  getConfig(): AppRouteConfigs;
  toReact(): ReactElement;
}

type RoutesAction = (routes: AppRoute[]) => AppRoute[];

type SectionRemovalAction = (section: string) => boolean;

type SectionDefinitions = Record<string, string>;

type Section = {
  definitions: SectionDefinitions;
  actions: RoutesAction[][];
};

type Sections = Record<string, Section>;

export class RoutesManager {
  private static sections: Sections = {};

  private static readonly removalActions: SectionRemovalAction[] = [];

  public static addSection(
    name: string,
    definitions: SectionDefinitions,
    action: RoutesAction,
    priority = 0
  ): void {
    if (priority < 0) {
      throw new Error("Priority must be greater than 0");
    }

    if (!RoutesManager.sections[name]) {
      RoutesManager.sections[name] = {
        definitions: {},
        actions: [],
      };
    }

    if (!RoutesManager.sections[name].actions[priority]) {
      RoutesManager.sections[name].actions[priority] = [];
    }

    RoutesManager.sections[name].definitions = merge(
      RoutesManager.sections[name].definitions,
      definitions
    );

    RoutesManager.sections[name].actions[priority].push(action);
  }

  public static removeSections(removalAction: SectionRemovalAction): void {
    RoutesManager.removalActions.push(removalAction);
  }

  public static getAllRoutes(): AppRoute[] {
    let sections: string[] = Object.keys(RoutesManager.sections);

    if (!isEmpty(RoutesManager.removalActions)) {
      RoutesManager.removalActions.forEach((removalAction) => {
        sections = sections.filter((section) => !removalAction(section));
      });
    }

    return sections.reduce((allRoutes: AppRoute[], section: string) => {
      if (!RoutesManager.sections[section]) {
        return allRoutes;
      }

      const sectionRoutes = compact(
        flatten(RoutesManager.sections[section].actions)
      ).reduce(
        (newRoutes: AppRoute[], action: RoutesAction) => action(newRoutes),
        []
      );

      return allRoutes.concat(sectionRoutes);
    }, []);
  }

  public static getRouteName(path: string): string | undefined {
    const route = this.getAllRoutes().find(
      (appRoute: AppRoute) => appRoute.getConfig().path === path
    );

    return route?.getConfig().screenName;
  }

  public static reset(): void {
    RoutesManager.sections = {};
    RoutesManager.removalActions.splice(0, RoutesManager.removalActions.length);
  }

  static generateReactRoutes(): ReactElement[] {
    return RoutesManager.getAllRoutes().map((appRoute: AppRoute) =>
      appRoute.toReact()
    );
  }

  static getRoute(name: string, route: string): string {
    if (!RoutesManager.sections[name].definitions[route]) {
      throw new Error("Route does not exist");
    }

    return RoutesManager.sections[name].definitions[route];
  }
}
