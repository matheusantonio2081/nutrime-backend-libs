export class ImageManager {
  private static images: Record<string, string> = {};

  public static add(name: string, image: string): void {
    ImageManager.images[name] = image;
  }

  public static get(name: string): string {
    if (!ImageManager.images[name]) {
      throw new Error(`Image ${name} not found`);
    }

    return ImageManager.images[name];
  }
}
