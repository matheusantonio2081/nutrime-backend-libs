import { assert } from "chai";

import { IconManager } from "./icon-manager";

describe("IconManager", function () {
  describe(".add", function () {
    before(() => {
      IconManager.add("icon", "icon-class");
    });

    it("Should be able to add icon", function () {
      assert.equal("icon-class", IconManager.get("icon"));
    });

    it("Should throw when icon is not found", function () {
      assert.throws(() => IconManager.get("missing-icon"));
    });
  });
});
