import faker from "faker";
import { assert } from "chai";
import { Builder } from "@nu-libs/entity-builder";

import { MenuManager, MenuItem } from "./menu-manager";

class MenuItemBuilder extends Builder<MenuItem, MenuItemBuilder> {
  public constructor() {
    super(MenuItemBuilder);
  }

  protected buildDefault(): MenuItem {
    return {
      label: faker.random.word(),
      icon: faker.random.word(),
      order: faker.datatype.number({
        min: 0,
        max: 10,
      }),
      screenName: faker.random.word(),
    };
  }
}

describe("MenuManager", function () {
  beforeEach(() => {
    MenuManager.reset();
  });

  describe(".addMenuItems", function () {
    it("Should insert menuItem on menu items", function () {
      const menuItems = new MenuItemBuilder().build();

      MenuManager.addMenuItems([menuItems]);

      const [firstMenuItem] = MenuManager.getMenuItems();

      assert.equal(firstMenuItem.label, menuItems.label);
    });
  });

  describe(".removeMenuItem", function () {
    it("Should remove menuItem from menu items", function () {
      const menuItems = new MenuItemBuilder().buildMany();
      const menuLength = menuItems.length;

      MenuManager.addMenuItems(menuItems);

      assert.equal(MenuManager.getMenuItems().length, menuLength);

      MenuManager.removeMenuItem(menuItems[0].screenName);

      assert.equal(MenuManager.getMenuItems().length, menuLength - 1);
    });
  });
});
