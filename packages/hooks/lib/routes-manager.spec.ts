import { assert } from "chai";
import React, { ReactElement } from "react";

import { RoutesManager, AppRoute, AppRouteConfigs } from "./routes-manager";

class MyAppRoute implements AppRoute {
  public constructor(
    public component: ReactElement,
    public configs: AppRouteConfigs
  ) {}

  public getConfig(): AppRouteConfigs {
    return this.configs;
  }

  public toReact(): ReactElement {
    return this.component;
  }
}

function appRouteConfigBuilder(
  path: string,
  screenName: string
): AppRouteConfigs {
  return {
    path,
    screenName,
    isSensible: false,
    isPrivate: true,
  };
}

describe("RoutesManager", function () {
  const homeSection = "home";

  const homeRoutesDefinition = {
    home: "/home",
    screenName: "home",
  };

  beforeEach(() => {
    RoutesManager.addSection(
      homeSection,
      homeRoutesDefinition,
      (baseRoutes: AppRoute[]): AppRoute[] => {
        const newRoutes = [
          new MyAppRoute(
            React.createElement("div"),
            appRouteConfigBuilder(
              homeRoutesDefinition.home,
              homeRoutesDefinition.screenName
            )
          ),
        ];

        return [...baseRoutes, ...newRoutes];
      },
      0
    )
  });

  afterEach(() => {
    RoutesManager.reset();
  });

  describe(".addSection", function () {
    it("Should throw when priority is lesser than 0", function () {
      assert.throws(
        () => RoutesManager.addSection("", {}, () => [], -1),
        "Priority must be greater than 0"
      );
    });
  });

  describe(".removeSections", function () {
    it("Should be able to add a remove section action", function () {
      assert.isTrue(RoutesManager.getAllRoutes().length === 1);

      RoutesManager.removeSections((section) => section === homeSection);

      assert.isTrue(RoutesManager.getAllRoutes().length === 0);
    });
  });

  describe(".getAllRoutes", function () {
    it("Should return array of routes", function () {
      const allRoutes = RoutesManager.getAllRoutes();

      assert.isTrue(allRoutes.length === 1);
      assert.equal(allRoutes[0].getConfig().path, homeRoutesDefinition.home);
    });
  });

  describe(".getRouteName", function () {
    it("Should get route name by path", function () {
      assert.equal(
        RoutesManager.getRouteName(homeRoutesDefinition.home),
        homeRoutesDefinition.screenName
      );
    });

    it("Should return undefined if route does not exists", function () {
      assert.equal(RoutesManager.getRouteName("/404"), undefined);
    });
  });

  describe(".generateReactRoutes", function () {
    it("Should be able to generate react routes", function () {
      const reactRoutes = RoutesManager.generateReactRoutes();
      assert.isTrue(reactRoutes.length === 1);
    });
  });

  describe(".getRoute", function () {
    it("Should get route by name and path", function () {
      assert.equal(
        RoutesManager.getRoute(
          homeRoutesDefinition.screenName,
          homeRoutesDefinition.screenName
        ),
        homeRoutesDefinition.home
      );
    });

    it("Should throw when route does not exists", function () {
      assert.throws(
        () =>
          RoutesManager.getRoute(
            homeRoutesDefinition.screenName,
            "missing-route"
          ),
        "Route does not exist"
      );
    });
  });
});
