module.exports = {
  extends: ["@nu-libs/eslint-config-base"],
  plugins: ["node"],
  rules: {
    // Core rules
    "no-restricted-imports": [
      "error",
      {
        patterns: ["./*", "../*"],
      },
    ],

    // Node rules
    "node/no-deprecated-api": ["error"],
  },
};
