# .eslintrc for React projects

This package configures .eslintrc with our default rules.

## Install

```
yarn add -D git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/eslint-config-react/<version>
```

## Using

After installing the dependencies, add this to your `.eslintrc.json`:

```
{
  "extends": [
    "@nu-libs/eslint-config-react"
    ...
  ]
  ...
}
```
