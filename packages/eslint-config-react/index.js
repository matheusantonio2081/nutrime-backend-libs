module.exports = {
  extends: ["@nu-libs/eslint-config-base"],
  plugins: ["unicorn", "react-hooks"],
  rules: {
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error",
    "unicorn/filename-case": [
      "error",
      {
        case: "kebabCase",
      },
    ],
    "@typescript-eslint/naming-convention": [
      "error",
      {
        selector: "default",
        format: ["camelCase", "UPPER_CASE", "PascalCase"],
      },
    ],
    "@typescript-eslint/no-extra-parens": [
      "error",
      "all",
      { ignoreJSX: "all" },
    ],
  },
};
