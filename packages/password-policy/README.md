# Password Policy

Class to help validate password

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/password-police/<version>
```

## Using

```typescript
import { PasswordPolicy } from "@nu-libs/password-policy";

new PasswordPolicy()
  .notAllowingString("forbiddenString")
  .withAtLeastOneLetter()
  .withAtLeastOneSpecialChar()
  .withAtLeastOneNumberDigit()
  .withLength({ min: 8, max: 10 })
  .validate("p@assw0rd");

//{
//  valid: true,
//  validationStepsResults: [
//    { valid: true, name: 'NotAllowingStringStep' },
//    { valid: true, name: 'WithAtLeastOneLetterStep' },
//    { valid: true, name: 'WithAtLeastOneSpecialCharStep' },
//    { valid: true, name: 'WithAtLeastOneNumberDigitStep' },
//    { valid: true, name: 'WithLengthStep' }
//  ]
//}
```
