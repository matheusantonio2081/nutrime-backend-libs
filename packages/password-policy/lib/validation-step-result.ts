export interface ValidationStepResult {
  valid: boolean,
  name: string,
}
