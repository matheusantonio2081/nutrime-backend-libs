import { ValidationStepResult} from "./validation-step-result";

export interface PasswordPolicyResult {
 valid: boolean,
 validationStepsResults: ValidationStepResult[]
}
