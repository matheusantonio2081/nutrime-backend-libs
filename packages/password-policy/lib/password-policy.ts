import { ValidationStep } from "./validation-step";
import { NotAllowingStringStep } from "./steps/not-allowing-string-step";
import { ValidationStepResult } from "./validation-step-result";
import { PasswordPolicyResult } from "./password-policy-result";
import { NumericStep } from "./steps/numeric-step";
import {
  NotAllowingConsecutiveCharsStep,
  NotAllowingConsecutiveCharsStepOptions,
} from "./steps/not-allowing-consecutive-chars-step";
import { NotAllowingDateStep } from "./steps/not-allowing-date-step";
import {
  NotAllowingRepeatedCharsStep,
  NotAllowingRepeatedCharsStepOptions,
} from "./steps/not-allowing-repeated-chars-step";
import { NotAllowingTheseCharsStep } from "./steps/not-allowing-these-chars-step";
import { WithAtLeastOneLetterStep } from "./steps/with-at-least-one-letter-step";
import { WithAtLeastOneNumberDigitStep } from "./steps/with-at-least-one-number-digit-step";
import { WithAtLeastOneSpecialCharStep } from "./steps/with-at-least-one-special-char-step";
import { WithLengthStep, WithLengthStepOptions } from "./steps/with-length-step";
import { NotAllowingPhoneStep } from "./steps/not-allowing-phone-step";

export class PasswordPolicy {
  private readonly validationSteps: ValidationStep[] = [];

  public notAllowingString(notAllowedString: string): PasswordPolicy {
    this.validationSteps.push(new NotAllowingStringStep(notAllowedString));
    return this;
  }

  public notAllowingConsecutiveChars(options: NotAllowingConsecutiveCharsStepOptions): PasswordPolicy {
    this.validationSteps.push(new NotAllowingConsecutiveCharsStep(options));
    return this;
  }

  public notAllowingRepeatedChars(options: NotAllowingRepeatedCharsStepOptions): PasswordPolicy {
    this.validationSteps.push(new NotAllowingRepeatedCharsStep(options));
    return this;
  }

  public notAllowingDate(notAllowedDate: Date): PasswordPolicy {
    this.validationSteps.push(new NotAllowingDateStep(notAllowedDate));
    return this;
  }

  public notAllowingPhone(notAllowedPhone: string): PasswordPolicy {
    this.validationSteps.push(new NotAllowingPhoneStep(notAllowedPhone));
    return this;
  }

  public notAllowingTheseChars(charsList: string[]): PasswordPolicy {
    this.validationSteps.push(new NotAllowingTheseCharsStep({ notAllowedCharsList: charsList }));
    return this;
  }

  public withAtLeastOneLetter(): PasswordPolicy {
    this.validationSteps.push(new WithAtLeastOneLetterStep());
    return this;
  }

  public withAtLeastOneNumberDigit(): PasswordPolicy {
    this.validationSteps.push(new WithAtLeastOneNumberDigitStep());
    return this;
  }

  public withAtLeastOneSpecialChar(): PasswordPolicy {
    this.validationSteps.push(new WithAtLeastOneSpecialCharStep());
    return this;
  }

  public withLength(options: WithLengthStepOptions): PasswordPolicy {
    this.validationSteps.push(new WithLengthStep(options));
    return this;
  }

  public numeric(): PasswordPolicy {
    this.validationSteps.push(new NumericStep());
    return this;
  }

  public validate(password: string): PasswordPolicyResult {
    if (this.validationSteps.length === 0) {
      throw new Error("No validations found");
    }
    const validationStepsResults: ValidationStepResult[] = [];
    this.validationSteps.forEach((step) => {
      validationStepsResults.push(step.validate(password));
    });

    const valid = validationStepsResults.every((result) => result.valid);
    return {
      valid,
      validationStepsResults,
    };
  }
}
