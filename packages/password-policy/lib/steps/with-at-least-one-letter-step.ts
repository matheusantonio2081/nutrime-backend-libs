import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class WithAtLeastOneLetterStep extends ValidationStep {
  public validate(password: string): ValidationStepResult {
    const regex = /[a-zA-Z]/;
    return {
      valid: regex.test(password),
      name: this.name,
    };
  }
}
