import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class NotAllowingPhoneStep extends ValidationStep {
  private readonly notAllowedPhone?: string;

  public constructor(phone: string) {
    super();

    const matches = phone.match(/\d{4}\-?\d{4}$/);

    if (matches) {
      this.notAllowedPhone = matches[0].replace(/\-/, "");
    }
  }

  public validate(password: string): ValidationStepResult {
    if (!this.notAllowedPhone) {
      return {
        valid: true,
        name: this.name,
      }
    }

    const cleanPassword = password.replace(/[\-\s]/gi, "");

    return {
      valid: !cleanPassword.includes(this.notAllowedPhone),
      name: this.name,
    };
  }
}
