import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class NotAllowingStringStep extends ValidationStep {
  public constructor(private readonly forbiddenString: string) {
    super();
  }

  public validate(password: string): ValidationStepResult {
    return {
      valid: !password.toLowerCase().includes(this.forbiddenString.toLowerCase()),
      name: this.name,
    };
  }
}
