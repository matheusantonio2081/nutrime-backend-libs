import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class WithAtLeastOneSpecialCharStep extends ValidationStep {
  public validate(password: string): ValidationStepResult {
    const regex = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return {
      valid: regex.test(password),
      name: this.name,
    };
  }
}
