import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export interface NotAllowingTheseCharsStepOptions {
  notAllowedCharsList: string[]
}

export class NotAllowingTheseCharsStep extends ValidationStep {
  public constructor(private readonly options: NotAllowingTheseCharsStepOptions) {
    super();
  }

  public validate(password: string): ValidationStepResult {
    return {
      valid: this.options.notAllowedCharsList.every(function (char) {
        return !password.toLowerCase().includes(char.toLowerCase());
      }),
      name: this.name,
    };
  }
}
