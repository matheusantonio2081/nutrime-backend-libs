import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class WithAtLeastOneNumberDigitStep extends ValidationStep {
  public validate(password: string): ValidationStepResult {
    const regex = /\d/;
    return {
      valid: regex.test(password),
      name: this.name,
    };
  }
}
