import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class NotAllowingDateStep extends ValidationStep {
  public constructor(private readonly notAllowedDate: Date) {
    super();
  }

  public validate(password: string): ValidationStepResult {
    const cleanPassword = password.replace(/[^\w]/gi, '')

    const result = this.possibleDateCombinations().every(function (dateFormat) {
      return !cleanPassword.includes(dateFormat.toLowerCase());
    });

    return {
      valid: result,
      name: this.name,
    };
  }

  private possibleDateCombinations(): string[] {
    const formats: string[] = [];

    this.buildDayFormats().forEach((day) => {
      this.buildMonthFormats().forEach((month) => {
        this.buildYearFormat().forEach((year) => {
          formats.push(`${day}${month}`);
          formats.push(`${month}${day}`);
          formats.push(`${day}${month}${year}`);
          formats.push(`${month}${day}${year}`);
          formats.push(`${year}${month}${day}`);
        });
      });
    });

    return [... new Set(formats)];
  }

  private buildDayFormats(): string[] {
    const day = this.notAllowedDate.getUTCDate();
    const dayLength = 2;
    return [... new Set([
      day.toString(),
      day.toString().padStart(dayLength, "0"),
    ])];
  }

  private buildMonthFormats(): string[] {
    const month = this.notAllowedDate.getUTCMonth() + 1;
    const monthLength = 2;
    return [... new Set([
      month.toString(),
      month.toString().padStart(monthLength, "0"),
    ])];
  }

  private buildYearFormat(): string[] {
    const year = this.notAllowedDate.getUTCFullYear();
    const abbreviateYearLength = 2;
    return [... new Set([
      year.toString(),
      year.toString().slice(-abbreviateYearLength),
    ])];
  }
}
