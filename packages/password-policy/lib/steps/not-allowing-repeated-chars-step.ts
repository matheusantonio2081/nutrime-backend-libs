import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export interface NotAllowingRepeatedCharsStepOptions {
  maxAllowedRepeatedChars: number
}

export class NotAllowingRepeatedCharsStep extends ValidationStep {
  public constructor(private readonly options: NotAllowingRepeatedCharsStepOptions) {
    super();
  }

  public validate(password: string): ValidationStepResult {
    return {
      valid: this.getMaxNumberOfRepeatedChars(password) <= this.options.maxAllowedRepeatedChars,
      name: this.name,
    };
  }

  private getMaxNumberOfRepeatedChars(password: string): number {
    let numberOfRepeatedChars = 1;
    let maxNumberOfRepeatedChars = 1;

    for (let i = 1; i < password.length; i++) {
      const previousChar = password[i - 1];
      const char = password[i];
      if (previousChar === char) {
        numberOfRepeatedChars += 1;
      } else {
        maxNumberOfRepeatedChars = Math.max(numberOfRepeatedChars, maxNumberOfRepeatedChars);
        numberOfRepeatedChars = 1;
      }
    }

    return Math.max(numberOfRepeatedChars, maxNumberOfRepeatedChars);
  }
}
