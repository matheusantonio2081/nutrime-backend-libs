import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export class NumericStep extends ValidationStep {
  public validate(password: string): ValidationStepResult {
    return {
      valid: /^\d+$/.test(password),
      name: this.name,
    };
  }
}
