import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export interface WithLengthStepOptions{
  min: number,
  max: number
}

export class WithLengthStep extends ValidationStep {
  public constructor(private readonly options: WithLengthStepOptions) {
    super();
  }

  public validate(password: string): ValidationStepResult {
    return {
      valid: password.length >= this.options.min && password.length <= this.options.max,
      name: this.name,
    };
  }
}
