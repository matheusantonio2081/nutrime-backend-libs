import { ValidationStep } from "../validation-step";
import { ValidationStepResult } from "../validation-step-result";

export interface NotAllowingConsecutiveCharsStepOptions {
  maxAllowedConsecutiveChars: number
}

export class NotAllowingConsecutiveCharsStep extends ValidationStep {
  public constructor(private readonly params: NotAllowingConsecutiveCharsStepOptions) {
    super();
  }

  public validate(password: string): ValidationStepResult {
    return {
      valid: this.getMaxNumberOfConsecutiveChars(password) <= this.params.maxAllowedConsecutiveChars,
      name: this.name,
    };
  }

  private getMaxNumberOfConsecutiveChars(password: string): number {
    let numberOfConsecutiveChars = 1;
    let maxNumberOfConsecutiveChars = 1;

    for (let i = 1; i < password.length; i++) {
      const previousChar = password[i - 1];
      const char = password[i];
      if (Math.abs(previousChar.charCodeAt(0) - char.charCodeAt(0)) === 1) {
        numberOfConsecutiveChars += 1;
      } else {
        maxNumberOfConsecutiveChars = Math.max(numberOfConsecutiveChars, maxNumberOfConsecutiveChars);
        numberOfConsecutiveChars = 1;
      }
    }

    return Math.max(numberOfConsecutiveChars, maxNumberOfConsecutiveChars);
  }
}
