import { ValidationStepResult } from "./validation-step-result";

export abstract class ValidationStep {
  abstract validate(password: string): ValidationStepResult;

  protected get name(): string {
    return this.constructor.name;
  }
}
