import { expect } from "chai";

import { PasswordPolicy } from "../lib/password-policy";

describe("PasswordPolicy", () => {
  it("should validate password", () => {
    const password = "p@ssword1";
    const forbiddenString = "pass";

    const policy = new PasswordPolicy()
      .withLength({ min: 8, max: 16 })
      .notAllowingString(forbiddenString)
      .notAllowingConsecutiveChars({ maxAllowedConsecutiveChars: 4 })
      .notAllowingRepeatedChars({ maxAllowedRepeatedChars: 4 })
      .notAllowingDate(new Date(2021, 2, 21))
      .notAllowingPhone("(11) 91234-5678")
      .notAllowingTheseChars(["/"])
      .withAtLeastOneLetter()
      .withAtLeastOneSpecialChar()
      .withAtLeastOneNumberDigit();

    const result = policy.validate(password);
    expect(result.valid).to.equal(true);
  });

  it("should validate numeric password", () => {
    const password = "12345678";

    const policy = new PasswordPolicy()
      .withLength({ min: 8, max: 8 })
      .numeric();

    const result = policy.validate(password);
    expect(result.valid).to.equal(true);
  });

  it("should throw a error if no validation is added", () => {
    const password = "12345678";
    const policy = new PasswordPolicy();

    expect(() => {
      policy.validate(password);
    }).to.throw(Error);
  });
});
