import { expect } from "chai";

import { WithAtLeastOneNumberDigitStep } from "../../lib/steps/with-at-least-one-number-digit-step";

describe("WithAtLeastOneNumberDigitStep", () => {
  it("when password contains at least one number, it should return result true", () => {
    const step = new WithAtLeastOneNumberDigitStep();

    const result = step.validate("password1");

    expect(result.valid).to.equal(true);
  });

  it("when password does not contains number, it should return result false", () => {
    const step = new WithAtLeastOneNumberDigitStep();

    const result = step.validate("password");

    expect(result.valid).to.equal(false);
  });
});
