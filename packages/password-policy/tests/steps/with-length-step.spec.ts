import { expect } from "chai";

import { WithLengthStep } from "../../lib/steps/with-length-step";

describe("WithLengthStep", () => {
  it("when the password has fewer characters than the minimum allowed, it should return result false", () => {
    const step = new WithLengthStep({ max: 8, min: 8 });

    const result = step.validate("123456");

    expect(result.valid).to.equal(false);
  });

  it("when the password has more characters than the maximum allowed, it should return result false", () => {
    const step = new WithLengthStep({ max: 4, min: 4 });

    const result = step.validate("123456");

    expect(result.valid).to.equal(false);
  });

  it("when the password has the the same number of chars than the max and min, it should return result true", () => {
    const step = new WithLengthStep({ max: 8, min: 8 });

    const result = step.validate("12345678");

    expect(result.valid).to.equal(true);
  });

  it("when the password is bigger than min and lower than max, it should return result true", () => {
    const step = new WithLengthStep({ max: 100, min: 8 });

    const result = step.validate("12345678");

    expect(result.valid).to.equal(true);
  });
});
