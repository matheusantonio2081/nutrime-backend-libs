import { expect } from "chai";

import { NotAllowingStringStep } from "../../lib/steps/not-allowing-string-step";

describe("NotAllowingString", () => {
  it("when password contains string, it should return result false", () => {
    const step = new NotAllowingStringStep("string");

    const result = step.validate("passtringword");

    expect(result.valid).to.equal(false);
  });

  it("when password contains string(case insensitive), it should return result false", () => {
    const step = new NotAllowingStringStep("StrinG");

    const result = step.validate("passtringword");

    expect(result.valid).to.equal(false);
  });

  it("when password not contains string, it should return result true", () => {
    const step = new NotAllowingStringStep("string");

    const result = step.validate("password");

    expect(result.valid).to.equal(true);
  });
});
