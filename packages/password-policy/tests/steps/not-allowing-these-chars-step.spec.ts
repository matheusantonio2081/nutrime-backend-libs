import { expect } from "chai";

import { NotAllowingTheseCharsStep } from "../../lib/steps/not-allowing-these-chars-step";

describe("NotAllowingTheseCharsStep", () => {
  it("when password contains not allowed char, it should return result false", () => {
    const step = new NotAllowingTheseCharsStep({ notAllowedCharsList: ["/", ">"] });

    const result = step.validate("12345/6");

    expect(result.valid).to.equal(false);
  });

  it("when password does not contains not allowed chars, it should return result true", () => {
    const step = new NotAllowingTheseCharsStep({ notAllowedCharsList: ["/", ">"] });

    const result = step.validate("123456ab?");

    expect(result.valid).to.equal(true);
  });
});
