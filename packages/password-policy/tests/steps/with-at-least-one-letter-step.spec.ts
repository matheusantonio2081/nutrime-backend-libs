import { expect } from "chai";

import { WithAtLeastOneLetterStep } from "../../lib/steps/with-at-least-one-letter-step";

describe("WithAtLeastOneLetterStep", () => {
  it("when password contains at least one letter, it should return result true", () => {
    const step = new WithAtLeastOneLetterStep();

    const result = step.validate("p123456");

    expect(result.valid).to.equal(true);
  });

  it("when password does not contains letter, it should return result false", () => {
    const step = new WithAtLeastOneLetterStep();

    const result = step.validate("123456");

    expect(result.valid).to.equal(false);
  });
});
