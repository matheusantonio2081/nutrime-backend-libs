import { expect } from "chai";

import { NotAllowingPhoneStep } from "../../lib/steps/not-allowing-phone-step";
import { ValidationStepResult } from "../../lib/validation-step-result";

type ValidationSteps = {
  withDigit: NotAllowingPhoneStep;
  withoutDigit: NotAllowingPhoneStep;
}

type ValidationResults = {
  resultWithDigit: ValidationStepResult;
  resultWithoutDigit: ValidationStepResult;
}

const buildSteps = (phone: string, prefix?: string): ValidationSteps => {
  return {
    withDigit: new NotAllowingPhoneStep(prefix + "9" + phone),
    withoutDigit: new NotAllowingPhoneStep(prefix + phone),
  }
};

const validateSteps = ({
  withDigit,
  withoutDigit,
}: ValidationSteps, password: string): ValidationResults => {
  return {
    resultWithDigit: withDigit.validate(password),
    resultWithoutDigit: withoutDigit.validate(password),
  }
}

const expectSteps = (results: ValidationResults, val: boolean) => {
  expect(results.resultWithDigit.valid).to.equal(val);
  expect(results.resultWithoutDigit.valid).to.equal(val);
};

describe("NotAllowingPhone", () => {
  it("should return false when the password contains the phone 912345678", () => {
    const steps = buildSteps("12345678", "(11) ");

    const results = validateSteps(steps, "912345678");

    expectSteps(results, false);
  });

  it("should return false when the password contains the phone 91234-5678", () => {
    const steps = buildSteps("12345678");

    const results = validateSteps(steps, "91234-5678");

    expectSteps(results, false);
  });

  it("should return false when the password contains the phone 9-1234-5678", () => {
    const steps = buildSteps("12345678", "(11) ");

    const results = validateSteps(steps, "9-1234-5678");

    expectSteps(results, false);
  });

  it("should return false when the password contains the phone 12345678", () => {
    const steps = buildSteps("12345678");

    const results = validateSteps(steps, "12345678");

    expectSteps(results, false);
  });

  it("should return false when the password contains the phone 1234-5678", () => {
    const steps = buildSteps("12345678", "+5511");

    const results = validateSteps(steps, "1234-5678");

    expectSteps(results, false);
  });

  it("should return true when the given phone is invalid", () => {
    const steps = buildSteps("DEADBEEF");

    const results = validateSteps(steps, "1234-5678");

    expectSteps(results, true);
  });
});
