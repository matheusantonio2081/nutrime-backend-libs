import { expect } from "chai";

import { WithAtLeastOneSpecialCharStep } from "../../lib/steps/with-at-least-one-special-char-step";

describe("WithAtLeastOneSpecialCharStep", () => {
  it("when password contains at least one letter, it should return result true", () => {
    const step = new WithAtLeastOneSpecialCharStep();

    const result = step.validate("p@ssword");

    expect(result.valid).to.equal(true);
  });

  it("when password does not contains letter, it should return result false", () => {
    const step = new WithAtLeastOneSpecialCharStep();

    const result = step.validate("123456");

    expect(result.valid).to.equal(false);
  });
});
