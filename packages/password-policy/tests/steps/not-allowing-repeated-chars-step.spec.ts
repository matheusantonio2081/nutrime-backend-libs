import { expect } from "chai";

import { NotAllowingRepeatedCharsStep } from "../../lib/steps/not-allowing-repeated-chars-step";

describe("NotAllowingRepeatedChars", () => {
  it("when password contains repeated chars in the middle, it should return result false", () => {
    const step = new NotAllowingRepeatedCharsStep({ maxAllowedRepeatedChars: 4 });

    const result = step.validate("passaaaaaword");

    expect(result.valid).to.equal(false);
  });

  it("when password contains repeated chars in the end, it should return result false", () => {
    const step = new NotAllowingRepeatedCharsStep({ maxAllowedRepeatedChars: 4 });

    const result = step.validate("passwordaaaaa");

    expect(result.valid).to.equal(false);
  });

  it("when password not contains repeated chars, it should return result true", () => {
    const step = new NotAllowingRepeatedCharsStep({ maxAllowedRepeatedChars: 4 });

    const result = step.validate("password");

    expect(result.valid).to.equal(true);
  });
});
