import { expect } from "chai";

import { NumericStep } from "../../lib/steps/numeric-step";

describe("NumericStep", () => {
  it("when password contains only digits, it should return result true", () => {
    const step = new NumericStep();

    const result = step.validate("123456");

    expect(result.valid).to.equal(true);
  });

  it("when password contains others chars, it should return result false", () => {
    const step = new NumericStep();

    const result = step.validate("123456ab");

    expect(result.valid).to.equal(false);
  });
});
