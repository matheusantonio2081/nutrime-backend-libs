import { expect } from "chai";

import {NotAllowingConsecutiveCharsStep} from "../../lib/steps/not-allowing-consecutive-chars-step";

describe("NotAllowingConsecutiveChars", () => {
  it("when password contains consecutive chars in the middle, it should return result false", () => {
    const step = new NotAllowingConsecutiveCharsStep({ maxAllowedConsecutiveChars: 4 });

    const result = step.validate("passabcdeword");

    expect(result.valid).to.equal(false);
  });

  it("when password contains consecutive chars in the end, it should return result false", () => {
    const step = new NotAllowingConsecutiveCharsStep({ maxAllowedConsecutiveChars: 4 });

    const result = step.validate("password12345");

    expect(result.valid).to.equal(false);
  });

  it("when password contains reverse consecutive chars, it should return result false", () => {
    const step = new NotAllowingConsecutiveCharsStep({ maxAllowedConsecutiveChars: 4 });

    const result = step.validate("password54321");

    expect(result.valid).to.equal(false);
  });

  it("when password not consecutive repeated chars, it should return result true", () => {
    const step = new NotAllowingConsecutiveCharsStep({ maxAllowedConsecutiveChars: 4 });

    const result = step.validate("password");

    expect(result.valid).to.equal(true);
  });
});
