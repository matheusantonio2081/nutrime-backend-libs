import { expect } from "chai";

import { NotAllowingDateStep } from "../../lib/steps/not-allowing-date-step";

describe("NotAllowingDate", () => {
  function buildDate(day: number, month: number, year: number) {
    return new Date(year, month - 1, day);
  }
  it("when password contains the date 10/10/1990, it should return result false", () => {
    const date = buildDate(10, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass10/10/1990word");

    expect(result.valid).to.equal(false);
  });

  it("when password contains the date 10/10/90, it should return result false", () => {
    const date = buildDate(10, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass10/10/90word");

    expect(result.valid).to.equal(false);
  });

  it("when password contains the date 101090, it should return result false", () => {
    const date = buildDate(10, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass101090word");

    expect(result.valid).to.equal(false);
  });

  it("when password contains the date 10-10, it should return result false", () => {
    const date = buildDate(10, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass10-10word");

    expect(result.valid).to.equal(false);
  });

  it("when password contains the date 1010, it should return result false", () => {
    const date = buildDate(10, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass1010word");

    expect(result.valid).to.equal(false);
  });

  it("when password contains the date 19901022, it should return result false", () => {
    const date = buildDate(22, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass19901022word");

    expect(result.valid).to.equal(false);
  })

  it("when password contains the date 19901022 with special characters, it should return result false", () => {
    const date = buildDate(22, 10, 1990);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass1990-10\\22word");

    expect(result.valid).to.equal(false);
  })

  it("when password contains the date 10/10/1994 with special characters, it should return result false", () => {
    const date = buildDate(10, 10, 1994);
    const step = new NotAllowingDateStep(date);

    const result = step.validate("pass10/10/1994word");

    expect(result.valid).to.equal(false);
  })
});
