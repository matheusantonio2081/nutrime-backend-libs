/* istanbul ignore file */
export { NavigationAdapter } from "./lib/navigation-adapter";
export { NullNavigationAdapter } from "./lib/null-navigation-adapter";
export { AppRoute } from "./lib/app-route";
export { AppRouteConfigs } from "./lib/app-route-configs";
