/* istanbul ignore file */
export abstract class NavigationAdapter {
  public update(_data: unknown): void {
    // This method is a noop because it's not required for an adapter to have an update method
  }

  public abstract reset(routes: string | string[]): void;
  public abstract isScreenFocused(): boolean;
  public abstract navigate(
    route: string,
    params?: Record<string, string>
  ): void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public abstract getParam(parameterName: string): any;
  public abstract goBack(): void;
  public abstract goBackTimes(count: number): void;
  public abstract getCurrentRoute(): string;
}
