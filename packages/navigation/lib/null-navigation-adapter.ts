import { NavigationAdapter } from "./navigation-adapter";

export class NullNavigationAdapter extends NavigationAdapter {
  public reset(_routes: string | string[]): void {
    // Null navigation adapter reset method is a noop
  }

  public isScreenFocused(): boolean {
    return false;
  }

  public navigate(_route: string, _params?: Record<string, string>): void {
    // Null navigation adapter navigate method is a noop
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public getParam(_parameterName: string): any {
    return null;
  }

  public goBack(): void {
    // Null navigation adapter goBack method is a noop
  }

  public goBackTimes(_count: number): void {
    // Null navigation adapter goBackTimes method is a noop
  }

  public getCurrentRoute(): string {
    return "";
  }
}
