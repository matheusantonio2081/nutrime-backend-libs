export interface AppRouteConfigs {
  isPrivate: boolean;
  isSensible: boolean;
  path: string;
  screenName: string;
}
