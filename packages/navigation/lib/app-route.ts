import { ReactElement } from "react";

import { AppRouteConfigs } from "./app-route-configs";

export interface AppRoute {
  getConfig(): AppRouteConfigs;
  toReact(): ReactElement;
}
