import { assert } from "chai";

import { NullNavigationAdapter } from "../index";

describe("NullNavigationAdapter", function () {
  it("can be instantiated and execute dummy methods", async function () {
    const navigationAdapter = new NullNavigationAdapter();

    assert.doesNotThrow(() => navigationAdapter.reset(""));
    assert.doesNotThrow(() => navigationAdapter.navigate(""));
    assert.doesNotThrow(() => navigationAdapter.goBack());
    assert.doesNotThrow(() => navigationAdapter.goBackTimes(0));
    assert.doesNotThrow(() => navigationAdapter.getCurrentRoute());
    assert.doesNotThrow(() => navigationAdapter.getParam(""));
    assert.doesNotThrow(() => navigationAdapter.isScreenFocused());
  });
});
