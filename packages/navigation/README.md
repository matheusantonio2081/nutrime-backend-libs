## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/navigation/<version>
```

## Using

```typescript
import {
  AppRoute,
  AppRouteConfigs,
  NavigationAdapter,
} from "@nu-libs/navigation";

class ReactRouterDomAdapter extends NavigationAdapter {
  ...
}
```

### Navigation Adapter

Navigation adapter exposes an abstract navigation class that implementations must extend to create adapters for libraries such as `react-router-dom` or `@react-navigation/native`.

Check `lib/null-navigation-adapter.ts` for an example implementation.

### AppRoute

AppRoute is the interface that [`RoutesManager`](https://gitlab.fretebras.com.br/fretepago/banking/banking-libs/tree/master/packages/hooks#routes-manager) accepts as a route.
