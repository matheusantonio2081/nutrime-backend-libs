# Base .eslintrc configuration

This package configures .eslintrc with our default rules.

## Install

```
yarn add -D git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/eslint-config-base/<version>
```

## Using

After installing the dependencies, add this to your `.eslintrc.json`:

```
{
  "extends": [
    "@nu-libs/eslint-config-base"
    ...
  ]
  ...
}
```
