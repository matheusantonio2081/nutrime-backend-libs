# tsconfig.json for Node projects

This configuration enforces that typescript will be compiled using the strict option.

It doesn't configure `outDir`, `baseDir` or `sourcemaps`.

### Setup

Install the library and its dependencies:

```
yarn add -D git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/tsconfig-node/<version>
```

This setup also uses `tslib` to reduce the compiled code size.

You must install it on your project as a normal dependency:

```
yarn add tslib
```

After installing the dependencies, add this to your `tsconfig.json`:

```
{
  "extends": "@nu-libs/tsconfig-node/tsconfig.json"
  ...
}
```
