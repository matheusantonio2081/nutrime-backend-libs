# tsconfig.json for Node projects

This configuration enforces that typescript will be compiled enabling the
`strict` option.

## Install

```
yarn add -D git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/tsconfig-lib/<version>
```

## Using

After installing the dependencies, add this to your `tsconfig.json`:

```
{
  "extends": "@nu-libs/tsconfig-lib/tsconfig.json"
  ...
}
```
