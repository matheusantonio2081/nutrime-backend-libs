#!/bin/bash

set -euo pipefail

production_build_dir_name=$1

rm -rf "$production_build_dir_name"
mkdir "$production_build_dir_name"

echo "copying lib files to $production_build_dir_name directory"
cp tsconfig.json "$production_build_dir_name"
cp package.json "$production_build_dir_name"
cp README.md "$production_build_dir_name"
