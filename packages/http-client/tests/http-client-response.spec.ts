/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable max-lines */
/* eslint-disable max-lines-per-function */
import * as z from "zod";
import nock from "nock";
import { assert } from "chai";
import { StatusCodes } from "http-status-codes";

import { HttpClient } from "../index";

const API_URL = "http://example.com";

describe("Http Client Response", function () {
  describe(".getData", function () {
    it("Should return with parsed response", async function () {
      const client = new HttpClient(API_URL);
      const mockResponse = { id: 1 };
      const fakeResponse = z.object({
        id: z.number(),
      });
      const fakeParser = (data: unknown) => fakeResponse.parse(data);
      const httpStatusOk = 200;

      nock(API_URL).post("/")
        .reply(httpStatusOk, mockResponse);

      const response = await client.request({
        url: "",
        method: "post",
      });

      const parsedResponse = await response.getData(fakeParser);

      assert.strictEqual(parsedResponse.id, mockResponse.id);
    });
  });

  describe(".hasError", function () {
    it("Should return TRUE when response could not be parsed", async function () {
      const client = new HttpClient(API_URL);
      const mockResponse = { id: "1" };
      const fakeResponse = z.object({
        id: z.number(),
      });
      const fakeParser = (data: unknown) => fakeResponse.parse(data);
      const fakeChecker = () => true;
      const httpStatusOk = 200;

      nock(API_URL).post("/")
        .reply(httpStatusOk, mockResponse);

      const response = await client.request({
        url: "",
        method: "post",
      });

      const hasError = await response.hasError(fakeParser, fakeChecker);

      assert.isTrue(hasError);
    });

    it("Should return FALSE when response could be parsed", async function () {
      const client = new HttpClient(API_URL);
      const mockResponse = { id: 1 };
      const fakeResponse = z.object({
        id: z.number(),
      });
      const fakeParser = (data: unknown) => fakeResponse.parse(data);
      const fakeChecker = () => true;
      const httpStatusOk = 200;

      nock(API_URL).post("/")
        .reply(httpStatusOk, mockResponse);

      const response = await client.request({
        url: "",
        method: "post",
      });

      const hasError = await response.hasError(fakeParser, fakeChecker);

      assert.isTrue(hasError);
    });
  });

  describe(".hasStatusOrThrow", function () {
    it("Should throw when status does not match", async function () {
      const client = new HttpClient(API_URL);
      const httpStatusOk = 201;

      nock(API_URL).post("/")
        .reply(httpStatusOk);

      const response = await client.request({
        url: "",
        method: "post",
      });

      response.hasStatusOrThrow(StatusCodes.CREATED);

      assert.throws(() => response.hasStatusOrThrow(StatusCodes.OK));
    });
  });

  describe(".getRawData", function () {
    it("Should return data as received", async function () {
      const client = new HttpClient(API_URL);
      const mockResponse = { id: "1" };
      const httpStatusOk = 200;

      nock(API_URL).post("/")
        .reply(httpStatusOk, mockResponse);

      const response = await client.request({
        url: "",
        method: "post",
      });

      const rawData = await response.getRawData();

      assert.isObject(rawData);
      assert.deepEqual(rawData, mockResponse);
    });
  });

  describe(".getHeader", function () {
    it("Should return header value", async function () {
      const client = new HttpClient(API_URL);
      const httpStatusOk = 200;

      nock(API_URL).post("/")
        .reply(httpStatusOk, {}, { authorization: "token" });

      const response = await client.request({
        url: "",
        method: "post",
      });

      const header = response.getHeader("authorization");

      assert(header === "token");
    });

    it("Should return empty header when header is not found", async function () {
      const client = new HttpClient(API_URL);
      const httpStatus = 200;

      nock(API_URL).post("/")
        .reply(httpStatus, {}, {});

      const response = await client.request({
        url: "",
        method: "post",
      });

      const header = response.getHeader("authorization");

      assert.isOk(header === "");
    });
  });
});
