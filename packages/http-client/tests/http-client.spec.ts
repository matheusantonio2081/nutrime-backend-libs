/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable max-nested-callbacks */
/* eslint-disable max-lines-per-function */
import nock from "nock";
import sinon from "sinon";
import chai, { assert } from "chai";
import chaiAsPromised from "chai-as-promised";
import { StatusCodes } from "http-status-codes";
import { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";

import { HttpClient } from "../index";

chai.use(chaiAsPromised);

const expect = chai.expect;

const API_URL = "http://example.com";

describe("Http Client", function () {
  describe(".constructor", function () {
    it("Should fail when creating an instance without baseUrl", function () {
      assert.throws(() => new HttpClient(""));
    });
  });

  describe(".request", function () {
    it("Should execute http request and returns", async function () {
      const client = new HttpClient(API_URL);

      nock(API_URL).post("/")
        .reply(StatusCodes.OK);

      const request = await client.request({
        url: "",
        method: "post",
      });

      assert.isOk(request.hasStatus(StatusCodes.OK));
    });

    it("Should return response when server returns an invalid status", async function () {
      const client = new HttpClient(API_URL);

      nock(API_URL).post("/")
        .reply(StatusCodes.INTERNAL_SERVER_ERROR);

      const response = await client.request({
        url: "",
        method: "post",
      });

      expect(response.statusCode).to.be.equal(StatusCodes.INTERNAL_SERVER_ERROR);
    });
  });

  describe(".listener", function () {
    it("Should execute request and response listeners", async function () {
      const spyRequest = sinon.spy();
      const spyResponse = sinon.spy();

      const listeners = {
        request: (request: AxiosRequestConfig): AxiosRequestConfig => {
          spyRequest();
          return request;
        },
        response: (response: AxiosResponse): AxiosResponse => {
          spyResponse();
          return response;
        },
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        error: (): void => {},
      };

      const client = new HttpClient(API_URL, listeners);

      nock(API_URL).post("/")
        .reply(StatusCodes.OK);

      await client.request({
        url: "",
        method: "post",
      });

      assert(spyRequest.calledOnce);
      assert(spyResponse.calledOnce);
    });

    it("Should call error listener", async function () {
      const spyError = sinon.spy();

      const listeners = {
        request: (request: AxiosRequestConfig): AxiosRequestConfig => request,
        response: (response: AxiosResponse): AxiosResponse => response,
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        error: (_error: AxiosError): void => spyError(),
      };

      const client = new HttpClient(API_URL, listeners);

      nock(API_URL).post("/")
        .reply(StatusCodes.INTERNAL_SERVER_ERROR);

      await expect(
        client.request({
          url: "",
          method: "post",
        })
      ).to.be.eventually.rejected.and.be.an.instanceOf(Error);

      assert(spyError.calledOnce);
    });
  });
});
