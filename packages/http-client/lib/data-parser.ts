export type DataParser<T> = (data: unknown) => Promise<T> | T;
