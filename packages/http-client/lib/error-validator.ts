export type ErrorValidator<T> = (error: T) => Promise<boolean> | boolean;
