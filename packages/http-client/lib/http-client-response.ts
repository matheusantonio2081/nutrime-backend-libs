import get from "lodash/get";

import { DataParser } from "./data-parser";
import { ErrorValidator } from "./error-validator";

export class HttpClientResponse {
  public constructor(
    public readonly statusCode: number,
    private readonly data: unknown,
    private readonly headers?: Record<string, string>,
  ) {}

  public hasStatus(status: number): boolean {
    return this.statusCode === status;
  }

  public hasStatusOrThrow(status: number): void {
    if (this.statusCode !== status) {
      throw new Error(`Request failed with status code ${this.statusCode}`);
    }
  }

  public async getData<T>(parse: DataParser<T>): Promise<T> {
    return parse(this.data);
  }

  public async getRawData(): Promise<unknown> {
    return this.data;
  }

  public async hasError<T>(parse: DataParser<T>, errorValidator: ErrorValidator<T>): Promise<boolean> {
    try {
      const data = await parse(this.data);

      return await errorValidator(data);
    } catch (error: unknown) {
      return true;
    }
  }

  public getHeader(key: string): string {
    return get(this.headers, key.toLowerCase(), "");
  }
}
