import { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

export interface HttpClientListener {
  request: (request: AxiosRequestConfig) => AxiosRequestConfig;
  response: (response: AxiosResponse) => AxiosResponse;
  error: (error: AxiosError) => void;
}
