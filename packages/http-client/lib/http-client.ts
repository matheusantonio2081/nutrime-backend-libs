import isEmpty from "lodash/isEmpty";
import isNil from "lodash/isNil";
import { StatusCodes } from "http-status-codes";
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";

import { HttpClientRequest } from "./http-client-request";
import { HttpClientResponse } from "./http-client-response";
import { HttpClientListener } from "./http-client-listener";

export class HttpClient {
  public listener: HttpClientListener = {
    request: (request: AxiosRequestConfig): AxiosRequestConfig => request,
    response: (response: AxiosResponse): AxiosResponse => response,
    error: (error: AxiosError): AxiosResponse | undefined => error.response,
  };

  protected readonly axios: AxiosInstance;

  public constructor(
    public readonly baseUrl: string,
    public readonly customListeners?: HttpClientListener
  ) {
    if (isEmpty(baseUrl)) {
      throw new Error("baseUrl is required");
    }

    if (!isNil(customListeners)) {
      this.listener = customListeners;
    }

    this.axios = axios.create({
      baseURL: baseUrl,
      validateStatus: this.isValidStatus.bind(this),
    });

    this.axios.interceptors.request.use(
      this.listener.request.bind(this),
      this.listener.error.bind(this)
    );

    this.axios.interceptors.response.use(
      this.listener.response.bind(this),
      this.listener.error.bind(this)
    );
  }

  public async request(
    request: HttpClientRequest
  ): Promise<HttpClientResponse> {
    const response = await this.axios.request(request);

    return new HttpClientResponse(
      response.status,
      response.data,
      response.headers
    );
  }

  protected isValidStatus(status: number): boolean {
    return status < StatusCodes.INTERNAL_SERVER_ERROR;
  }
}
