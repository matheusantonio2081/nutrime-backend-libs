# Http Client

Base HTTP client class.

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/http-client/<version>
```

## Using

`getData` parses the data according with the provided `parse` callback parameter.

`getRawData` gets the response data without safe typing.

`hasError` parses the error object according with the provided `parse` callback
parameter and determines the error based on the `errorValidator` calback parameter.

### Example using Zod (https://www.npmjs.com/package/zod)

```typescript
import * as z from "zod";
import { HttpClient } from "@nu-libs/http-client";

const client = new HttpClient("https://vizirbank.com");

const responseType = z.object({
  balance: z.number(),
});

const request = await client.request({
  url: "/balance",
  method: "get",
});

await request.getData(responseType.parse);
```

Usage example with custom request listeners:

```typescript
import * as z from "zod";
import { HttpClient } from "@nu-libs/http-client";

const client = new HttpClient("https://vizirbank.com", {
  request: (request: AxiosRequestConfig): AxiosRequestConfig => {
    myCustomRequestFunction();
    return request;
  },
  response: (response: AxiosResponse): AxiosResponse => response,
  error: (): void => {},
});

await client.request({
  url: "/balance",
  method: "get",
});
```
