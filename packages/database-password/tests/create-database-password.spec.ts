import faker from "faker";
import { expect } from "chai";

import { CreateDatabasePassword } from "../lib/create-database-password";
import { InMemoryPasswordStrategy } from "./in-memory-password-strategy";

describe("CreateDatabasePassword", () => {
  const password = faker.internet.password();

  process.env.DATABASE_PASSWORD = password;
  process.env.AWS_REGION = "us-east-1";
  process.env.DATABASE_USER = faker.internet.userName();
  process.env.DATABASE_HOST = faker.internet.domainName();
  process.env.DATABASE_PORT = String(faker.internet.port());

  it("should return the get method if ENABLE_RDS_IAM_AUTHENTICATION is false", async () => {
    process.env.ENABLE_RDS_IAM_AUTHENTICATION = "false";

    const rdsStrategy = new CreateDatabasePassword(
      new InMemoryPasswordStrategy(password),
      new InMemoryPasswordStrategy(password)
    );

    const generateToken = await rdsStrategy.get();
    expect(generateToken.password).to.be.eq(password);
  });

  it("should return the get method if ENABLE_RDS_IAM_AUTHENTICATION is true", async () => {
    process.env.ENABLE_RDS_IAM_AUTHENTICATION = "true";

    const rdsStrategy = new CreateDatabasePassword(
      new InMemoryPasswordStrategy(password),
      new InMemoryPasswordStrategy(password)
    );

    const generateToken = await rdsStrategy.get();
    expect(generateToken.password).to.be.eq(password);
  });
});
