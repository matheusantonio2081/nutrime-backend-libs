import { expect } from "chai";
import faker from "faker";
import moment from "moment";

import { RDSDatabasePassword } from "../lib/rds-database-password";

describe("RDSDatabasePassword", () => {
  it("should return the method isExpired", async () => {
    const token = faker.internet.password();
    const expiresAt = moment().toDate();
    expect(new RDSDatabasePassword(token, expiresAt).isExpired()).to.be.eq(false);
  });
});
