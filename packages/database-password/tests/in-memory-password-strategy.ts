import { DatabasePassword } from "../lib/database-password";
import { DefaultPasswordStrategy } from "../lib/default-database-password-strategy";

export class InMemoryPasswordStrategy implements DefaultPasswordStrategy {
  public constructor(private readonly password: string) {
  }

  public async get(): Promise<DatabasePassword> {
    return new DatabasePassword(this.password);
  }
}
