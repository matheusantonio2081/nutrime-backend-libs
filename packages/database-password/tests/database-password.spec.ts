import { expect } from "chai";
import faker from "faker";

import { DatabasePassword } from "../lib/database-password";

describe("DatabasePassword", () => {
  it("should return the method isExpired", async () => {
    const password = faker.internet.password();
    expect(new DatabasePassword(password).isExpired()).to.be.eq(false);
  });
});
