import { expect } from "chai";
import faker from "faker";

import { DefaultPasswordStrategy } from "../lib/default-database-password-strategy";

describe("DefaultPasswordStrategy", () => {
  it("should return correctly the method get", async () => {
    const password = faker.internet.password();
    process.env.DATABASE_PASSWORD = password;
    const defaultStrategy = await new DefaultPasswordStrategy().get();
    expect(defaultStrategy.password).to.be.eq(password);
  });
});
