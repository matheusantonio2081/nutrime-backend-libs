import { expect } from "chai";
import faker from "faker";

import { RDSDatabasePasswordStrategy } from "../lib/rds-database-password-strategy";

describe("RDSDatabasePasswordStrategy", () => {
  it("should return correctly the method get", async () => {
    process.env.AWS_ACCESS_KEY_ID = faker.internet.password();
    process.env.AWS_SECRET_ACCESS_KEY = faker.internet.password();

    const rdsStrategy = await new RDSDatabasePasswordStrategy().get();
    expect(rdsStrategy.isExpired()).to.be.eq(false);
  });
});
