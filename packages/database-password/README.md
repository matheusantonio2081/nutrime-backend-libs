# Database Password

This package creates the database password according to the environment. If you want to use RDS IAM authentication you should pass the env `ENABLE_RDS_IAM_AUTHENTICATION` as `true`.

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/database-password/<version>
```

## Using

By default, AWS use token's TTL as 15 minutes and it's date expiration is not returned. For this, as we need to deal with this from our side, we use 10 minutes to have a safety margin.

Usage example:

```typescript
import { CreateDatabasePassword } from "@nu-libs/database-password";

const password = await new CreateDatabasePassword().get();
```
