import assert from "assert";

export const getEnvOrThrow = <T>(val: T | undefined): T => {
  assert(val !== undefined);
  return val;
};
