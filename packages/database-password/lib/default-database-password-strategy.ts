import { DatabasePassword } from "./database-password";
import { GetDatabasePasswordStrategy } from "./get-database-password-strategy";
import { getEnvOrThrow } from "./get-env-or-throw";

export class DefaultPasswordStrategy implements GetDatabasePasswordStrategy {
  public async get(): Promise<DatabasePassword> {
    return new DatabasePassword(getEnvOrThrow(process.env.DATABASE_PASSWORD));
  }
}
