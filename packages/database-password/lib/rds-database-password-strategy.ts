import { promisify } from "util";

import { Signer } from "aws-sdk/clients/rds";
import moment from "moment";

import { getEnvOrThrow } from "./get-env-or-throw";
import { DatabasePassword } from "./database-password";
import { GetDatabasePasswordStrategy } from "./get-database-password-strategy";
import { RDSDatabasePassword } from "./rds-database-password";

export const TIME_TO_LIVE = 10;

export class RDSDatabasePasswordStrategy implements GetDatabasePasswordStrategy {
  private readonly signerToken = new Signer({
    region: getEnvOrThrow(process.env.AWS_REGION),
    username: getEnvOrThrow(process.env.DATABASE_USER),
    hostname: getEnvOrThrow(process.env.DATABASE_HOST),
    port: Number(getEnvOrThrow(process.env.DATABASE_PORT)),
  });

  public async get(): Promise<DatabasePassword> {
    const getAuthTokenAsync: (options: Signer.SignerOptions) => Promise<string> =
      promisify(this.signerToken.getAuthToken.bind(this.signerToken));
    const token = await getAuthTokenAsync({});
    const expiresAt = moment().add(TIME_TO_LIVE, "minutes")
      .toDate();

    return new RDSDatabasePassword(token, expiresAt);
  }
}
