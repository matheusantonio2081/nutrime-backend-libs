import { DatabasePassword } from "./database-password";
import { DefaultPasswordStrategy } from "./default-database-password-strategy";
import { GetDatabasePasswordStrategy } from "./get-database-password-strategy";
import { RDSDatabasePasswordStrategy } from "./rds-database-password-strategy";

export class CreateDatabasePassword {
  public constructor(
    private readonly defaultStrategy: GetDatabasePasswordStrategy = new DefaultPasswordStrategy(),
    private readonly iamStrategy: GetDatabasePasswordStrategy = new RDSDatabasePasswordStrategy(),
  ) {

  }

  public async get(): Promise<DatabasePassword> {
    if (process.env.ENABLE_RDS_IAM_AUTHENTICATION === "true") {
      return this.iamStrategy.get();
    }
    return this.defaultStrategy.get();
  }
}

