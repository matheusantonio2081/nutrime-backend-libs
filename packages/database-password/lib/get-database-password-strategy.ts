import { DatabasePassword } from "./database-password";

export interface GetDatabasePasswordStrategy {
  get(): Promise<DatabasePassword>
}

