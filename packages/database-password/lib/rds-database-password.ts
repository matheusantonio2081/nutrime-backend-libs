import { DatabasePassword } from "./database-password";

export class RDSDatabasePassword extends DatabasePassword {
  public constructor(token: string, private readonly expiresAt: Date) {
    super(token);
  }

  public isExpired(): boolean {
    return new Date() > this.expiresAt;
  }
}
