import { createRequest, createResponse } from "node-mocks-http";
import { assert, spy } from "sinon";
import { StatusCodes } from "http-status-codes";
import { expect } from "chai";
import { HttpError } from "routing-controllers";
import faker from "faker";

import { HttpErrorMiddleware } from "../lib/http-error-middleware";

describe("HttpErrorMiddleware", () => {
  describe("when error is not a instance of HttpError", () => {
    it("should return internal server error", () => {
    // given
      const middleware = new HttpErrorMiddleware();

      const next = spy();

      const error = new Error();
      const request = createRequest({
        method: "POST",
        url: faker.internet.url(),
      });
      const response = createResponse();

      // when
      middleware.error(error, request, response, next);

      // then
      expect(response.statusCode).to.equal(StatusCodes.INTERNAL_SERVER_ERROR);
      expect(response._getData()).to.be.deep.equal({
        code: "internal_error",
        message: "An unexpected error occurred during the request.",
      });

      assert.notCalled(next);
    });
  });

  describe("when error is a instance of HttpError", () => {
    it("should return the given httpCode and properties", () => {
    // given
      const middleware = new HttpErrorMiddleware();

      const next = spy();

      const error = new HttpError(StatusCodes.BAD_REQUEST, "Bad request");
      const request = createRequest({
        method: "POST",
        url: faker.internet.url(),
      });
      const response = createResponse();

      // when
      middleware.error(error, request, response, next);

      // then
      expect(response.statusCode).to.equal(StatusCodes.BAD_REQUEST);
      expect(response._getData()).to.be.deep.equal({
        message: "Bad request",
      });

      assert.notCalled(next);
    });
  });
});
