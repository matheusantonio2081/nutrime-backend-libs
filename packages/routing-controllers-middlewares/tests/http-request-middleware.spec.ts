import { Logger } from "@nu-libs/logger";
import { createRequest, createResponse } from "node-mocks-http";
import { assert, spy, useFakeTimers } from "sinon";
import faker from "faker";

import { HttpRequestMiddleware } from "../lib/http-request-middleware";

describe("HttpRequestMiddleware", () => {
  const clock = useFakeTimers();

  after(() => {
    clock.restore();
  });

  it("should log request", () => {
    // given
    const transporter = {
      log: spy(),
    };

    Logger.useTransporter(transporter);

    const middleware = new HttpRequestMiddleware();

    const next = spy();

    const request = createRequest({
      method: "POST",
      url: faker.internet.url(),
      headers: {},
      params: {},
      body: {},
    });
    const response = createResponse();

    // when
    middleware.use(request, response, next);

    // then
    assert.calledWithMatch(transporter.log, {
      message: `Request POST ${request.url}`,
    });
    assert.calledOnce(next);
  });

  it("should stringify body when request content type is json", () => {
    // given
    const transporter = {
      log: spy(),
    };

    Logger.useTransporter(transporter);

    const middleware = new HttpRequestMiddleware();

    const next = spy();

    const request = createRequest({
      method: "POST",
      url: faker.internet.url(),
      headers: {
        "content-type": "application/json",
      },
      params: {},
      body: {
        name: faker.name.findName(),
      },
    });
    const response = createResponse();

    // when
    middleware.use(request, response, next);

    // then
    assert.calledWithMatch(transporter.log, {
      message: `Request POST ${request.url}`,
      recordMetadata: {
        data: JSON.stringify(request.body),
      },
    });
    assert.calledOnce(next);
  });
});
