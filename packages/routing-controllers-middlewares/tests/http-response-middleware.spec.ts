import { Logger } from "@nu-libs/logger";
import { createRequest, createResponse } from "node-mocks-http";
import { spy, assert } from "sinon";
import faker from "faker";
import { StatusCodes } from "http-status-codes";

import { HttpResponseMiddleware } from "../lib/http-response-middleware";

describe("HttpResponseMiddleware", () => {
  it("should log response when response.write is called", () => {
    // given
    const transporter = {
      log: spy(),
    };

    Logger.useTransporter(transporter);

    const middleware = new HttpResponseMiddleware();

    const next = spy();
    const request = createRequest({
      method: "POST",
      url: faker.internet.url(),
    });
    const response = createResponse({});

    response.status(StatusCodes.OK);
    response.setHeader("content-type", "application/json");

    const data = { name: faker.name.findName() };

    // when
    middleware.use(request, response, next);

    response.write(JSON.stringify(data));
    response.end();

    // then
    assert.calledWithMatch(transporter.log, {
      message: `Response POST ${request.url} 200`,
      recordMetadata: {
        data: JSON.stringify(data),
      },
    });
    assert.calledOnce(next);
  });
});
