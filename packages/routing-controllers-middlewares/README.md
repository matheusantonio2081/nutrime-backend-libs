# @nu-libs/routing-controllers-middlewares

This package contains essential middleware for using `routing-controllers`.

- `HttpRequestMiddleware`: Log incoming requests.
- `HttpResponseMiddleware`: Log outgoing responses.
- `HttpErrorMiddleware`: Intercept unhandled errors and return safe messages while log error on console.

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/routing-controllers-middlewares/<version>
```

### Usage

```typescript
  import { Container } from "inversify";
  import { useExpressServer } from "routing-controllers";
  import express from "express";

  import { HttpRequestMiddleware, HttpResponseMiddleware, HttpErrorMiddleware } from "@nu-libs/routing-controllers-middlewares";

  const app = express();

  useExpressServer(app, {
    ...,
    defaultErrorHandler: false,
    middlewares: [HttpRequestMiddleware, HttpResponseMiddleware, HttpErrorMiddleware],
  });
```
