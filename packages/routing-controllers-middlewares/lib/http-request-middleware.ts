import { NextFunction, Request, Response } from "express";
import { injectable } from "inversify";
import { ExpressMiddlewareInterface, Middleware } from "routing-controllers";
import { Logger } from "@nu-libs/logger";

@injectable()
@Middleware({ type: "before" })
export class HttpRequestMiddleware implements ExpressMiddlewareInterface {
  private readonly logger = Logger.getLogger(HttpRequestMiddleware.name);

  public use(request: Request, _response: Response, next: NextFunction): void {
    this.logger.info(`Request ${request.method.toUpperCase()} ${request.url}`, {
      url: request.url,
      method: request.method,
      headers: request.headers,
      params: request.params,
      data:
        request.header("content-type")?.includes("application/json") ?? false
          ? JSON.stringify(request.body)
          : "",
    });

    next();
  }
}
