import { Logger } from "@nu-libs/logger";
import { Request, Response, NextFunction } from "express";
import { StatusCodes } from "http-status-codes";
import { injectable } from "inversify";
import {
  ExpressErrorMiddlewareInterface,
  HttpError,
  Middleware,
} from "routing-controllers";

import { serializeError } from "./utils/serialize-error";

@injectable()
@Middleware({ type: "after" })
export class HttpErrorMiddleware implements ExpressErrorMiddlewareInterface {
  private readonly logger = Logger.getLogger(HttpErrorMiddleware.name);

  // eslint-disable-next-line max-params
  public error(
    error: Error,
    request: Request,
    response: Response,
    _next: NextFunction
  ): Response {
    this.logger.info(`Error ${request.method.toUpperCase()} ${request.url}`, {
      error: serializeError(error),
    });

    if (!this.isHttpError(error)) {
      return response.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
        code: "internal_error",
        message: "An unexpected error occurred during the request.",
      });
    }

    const { httpCode, ...rest } = error as HttpError;

    return response.status(httpCode).send(rest);
  }

  private isHttpError(error: Error) {
    return error instanceof HttpError;
  }
}
