/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/strict-boolean-expressions */
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable complexity */
/* eslint-disable prefer-rest-params */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable max-lines-per-function */
import { Logger } from "@nu-libs/logger";
import { NextFunction, Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { injectable } from "inversify";
import { ExpressMiddlewareInterface, Middleware } from "routing-controllers";

@injectable()
@Middleware({ type: "before" })
export class HttpResponseMiddleware implements ExpressMiddlewareInterface {
  private readonly logger: Logger = Logger.getLogger(
    HttpResponseMiddleware.name
  );

  public use(request: Request, response: Response, next: NextFunction): any {
    const defaultEnd = response.end as Function;
    const defaultWrite = response.write as Function;

    const chunks: Buffer[] = [];

    response.write = function write(chunk: any): any {
      chunks.push(Buffer.from(chunk));

      return defaultWrite.apply(response, arguments);
    };

    const that = this;

    response.end = function end(chunk: any): any {
      if (chunk) {
        chunks.push(Buffer.from(chunk));
      }

      const message = `Response ${request.method.toUpperCase()} ${
        request.url
      } ${response.statusCode}`;
      const extra = {
        url: request.url,
        method: request.method,
        headers: { ...response.getHeaders() },
        status: response.statusCode,
        data: that.isJSON(response)
          ? Buffer.concat(chunks).toString("utf8")
          : "",
      };

      that.isError(response.statusCode)
        ? that.logger.error(message, extra)
        : that.logger.info(message, extra);

      return defaultEnd.apply(response, arguments);
    };

    next();
  }

  private isError(statusCode: number) {
    return statusCode >= StatusCodes.BAD_REQUEST;
  }

  private isJSON(response: Response) {
    const contentType = response.getHeader("content-type");

    return typeof contentType === "string"
      ? contentType.includes("application/json")
      : false;
  }
}
