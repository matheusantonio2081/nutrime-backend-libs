# Logger winston transporter

Logger transporter configuration using [winston](https://github.com/winstonjs/winston).

This logger has support to deliver logs without blocking the event loop to
the _console_ and/or _elasticsearch_.

_Note:_ this library is intended to be used on Node applications where
the process lifecycle can be controlled. DON'T USE THIS ON AWS LAMBDA!

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/logger-winston-transporter/<version>
```

## Using

Initialize the _logger_ with _logger-winston-transporter_:

```typescript
import { Logger } from "@nu-libs/logger";
import { WinstonTransporter } from "@nu-libs/logger-winston-transporter";

Logger.useTransporter(new WinstonTransporter("console:info:pretty"));

const logger = Logger.getLogger();
logger.info("Hello", { world: "!" });
```

### Configuration

#### spec

Configures the log destination, level and format (when supported), on the
format: `{destination}:{level}:{~format}`.

- Possible values for **destination**: `console` or `elasticsearch`
- Possible values for **level**: `info` or `error`
- Possible values for **format**: `pretty` or `json`

Examples:

```typescript
// send info and error logs to console, with a readable format
Logger.useTransporter(new WinstonTransporter("console:info:pretty"));

// send only error logs to console, with json format
Logger.useTransporter(new WinstonTransporter("console:error:json"));

// send logs to "elasticsearch", note: this destination can't be configured with a format
Logger.useTransporter(new WinstonTransporter("elasticsearch:info"));

// multiple destinations
Logger.useTransporter(
  new WinstonTransporter("console:error:pretty,elasticsearch:info")
);
```

#### options.elasticsearch.url

_Optional, defaults to http://localhost:9200._

Elasticsearch endpoint url.

Note: this module is prepared to connect on AWS's Elasticsearch Service domains
using AWS's environment variables `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_REGION`.

Example:

```typescript
Logger.useTransporter(new WinstonTransporter("elasticsearch:info"), {
  elasticsearch: {
    endpoint: "https://myelasticsearchcluster.com",
  },
});
```

#### options.elasticsearch.indexPrefix

_Optional, defaults to "logger"._

Example:

```typescript
// indices will be created as "my-logs-2021-04-01"
Logger.useTransporter(new WinstonTransporter("elasticsearch:info"), {
  elasticsearch: {
    indexPrefix: "my-logs",
  },
});
```

#### options.elasticsearch.flushInterval

_Optional. defaults to 3000_

Interval, in **ms**, where logs will be flushed to `elasticsearch`.

Example:

```typescript
Logger.useTransporter(new WinstonTransporter("elasticsearch:info"), {
  elasticsearch: {
    flushInterval: 10000,
  },
});
```
