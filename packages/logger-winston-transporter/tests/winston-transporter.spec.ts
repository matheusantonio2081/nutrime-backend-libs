import { expect } from "chai";

import { WinstonTransporter } from "../index";

describe("WinstonTransporter", function () {
  it("boot check", function () {
    const transporter = new WinstonTransporter("console:info:pretty");
    expect(transporter).to.be.instanceOf(WinstonTransporter);
  });
});
