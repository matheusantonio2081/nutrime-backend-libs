import Transport from "winston-transport";

export class NullWinstonTransport extends Transport {
  public readonly name = "NullTransport";

  public log(_info: unknown, next: () => void): Transport {
    return this.noop(next);
  }

  public logv(_info: unknown, next: () => void): Transport {
    return this.noop(next);
  }

  public close(): void {
    /* Nothing to do */
  }

  private noop(next: () => void): Transport {
    next();
    return this;
  }
}
