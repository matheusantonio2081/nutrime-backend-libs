import debug from "debug";
import winston from "winston";
import AWS from "aws-sdk";
import createAwsElasticsearchConnector from "aws-elasticsearch-connector";
import { ElasticsearchTransport } from "winston-elasticsearch";
import { LogRecord, Transporter } from "@nu-libs/logger";

import { NullWinstonTransport } from "./null-winston-transport";
import { WinstonTransporterSpecParser } from "./winston-transporter-spec-parser";
import {
  WinstonTransporterOptions,
  PartialWinstonTransporterOptions,
} from "./winston-transporter-options";

const d = debug("@nu-libs/logger-winston-transporter");

export class WinstonTransporter implements Transporter {
  private readonly defaultOptions: WinstonTransporterOptions = {
    elasticsearch: {
      endpoint: "http://localhost:9200",
      indexPrefix: "logger",
      flushInterval: 3000,
    },
  };

  private readonly winston: winston.Logger;

  private readonly spec: WinstonTransporterSpecParser;

  private readonly options: WinstonTransporterOptions;

  public constructor(
    spec: string,
    options: PartialWinstonTransporterOptions = {}
  ) {
    this.spec = new WinstonTransporterSpecParser(spec);
    this.options = {
      elasticsearch: Object.assign(
        {},
        this.defaultOptions.elasticsearch,
        options.elasticsearch
      ),
    };
    this.winston = this.buildWinstonLogger();
  }

  public log(record: LogRecord): void {
    this.winston.log(record.level, record.message, {
      ...record.metadata,
      ...record.contextMetadata,
      name: record.name,
      severity: record.level,
      meta: {
        [record.name]: record.recordMetadata,
      },
    });
  }

  // eslint-disable-next-line complexity
  private buildWinstonLogger(): winston.Logger {
    const logger = winston.createLogger();

    if (this.spec.has("null")) {
      logger.add(new NullWinstonTransport());
    }

    if (this.spec.has("console")) {
      logger.add(
        this.createConsoleWinstonTransport(
          this.spec.getLevel("console"),
          this.spec.getFormat("console")
        )
      );
    }

    if (this.spec.has("elasticsearch")) {
      logger.add(
        this.createElasticsearchWinstonTransport(
          this.spec.getLevel("elasticsearch")
        )
      );
    }

    return logger;
  }

  private createConsoleWinstonTransport(
    level: string,
    format: string
  ): winston.transports.ConsoleTransportInstance {
    return new winston.transports.Console({
      level,
      format:
        format === "json"
          ? winston.format.combine()
          : winston.format.combine(
              winston.format.timestamp(),
              winston.format.printf((log) =>
                [
                  // eslint-disable-next-line @typescript-eslint/no-magic-numbers
                  log.timestamp,
                  `[${log.level}]`,
                  log.message,
                  JSON.stringify(log, null, 4),
                ].join(" ")
              )
            ),
    });
  }

  private createElasticsearchWinstonTransport(
    level: string
  ): ElasticsearchTransport {
    const elasticsearchTransport = new ElasticsearchTransport({
      level,
      format: winston.format.combine(),
      flushInterval: this.options.elasticsearch.flushInterval,
      indexPrefix: this.options.elasticsearch.indexPrefix,
      clientOpts: {
        ...createAwsElasticsearchConnector(AWS.config),
        node: this.options.elasticsearch.endpoint,
      },
    });

    elasticsearchTransport.on("error", (error) => d(error));

    return elasticsearchTransport;
  }
}
