declare module "aws-elasticsearch-connector" {
  import type AWS from "aws-sdk";
  import type { Connection, Transport } from "@elastic/elasticsearch";

  interface ElasticsearchConnector {
    Connection: typeof Connection; // eslint-disable-line @typescript-eslint/naming-convention
    Transport: typeof Transport; // eslint-disable-line @typescript-eslint/naming-convention
  }

  // eslint-disable-next-line import/no-default-export
  export default function createAwsElasticsearchConnector(awsConfig: AWS.Config): ElasticsearchConnector;
}
