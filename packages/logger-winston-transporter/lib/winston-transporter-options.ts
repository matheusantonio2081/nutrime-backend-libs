type DeepPartialObject<T> = {
  [P in keyof T]?: DeepPartialObject<T[P]>
};

interface ElasticsearchTransporterOptions {
  endpoint: string;
  indexPrefix: string;
  flushInterval: number;
}

export interface WinstonTransporterOptions {
  elasticsearch: ElasticsearchTransporterOptions;
}

export type PartialWinstonTransporterOptions = DeepPartialObject<WinstonTransporterOptions>;
