type Level = "info" | "error";
type Format = "pretty" | "json";

export class WinstonTransporterSpecParser {
  private readonly transporters = new Map<string, {
    level: Level;
    format: Format;
  }>();

  private readonly defaultLevel = "error";

  private readonly defaultFormat = "json";

  public constructor(spec: string) {
    this.parse(spec);
  }

  public has(transporter: string): boolean {
    return this.transporters.has(transporter);
  }

  public getLevel(transporter: string): Level {
    const spec = this.transporters.get(transporter);

    if (spec === undefined) {
      return this.defaultLevel;
    }

    return spec.level;
  }

  public getFormat(transporter: string): Format {
    const spec = this.transporters.get(transporter);

    if (spec === undefined) {
      return this.defaultFormat;
    }

    return spec.format;
  }

  private isSeverity(level: string): level is Level {
    return ["info", "error"].includes(level);
  }

  private getLevelOrDefault(specLevel: string): Level {
    return this.isSeverity(specLevel) ? specLevel : this.defaultLevel;
  }

  private isFormat(format: string): format is Format {
    return ["json", "pretty"].includes(format);
  }

  private getFormatOrDefault(specFormat: string): Format {
    return this.isFormat(specFormat) ? specFormat : this.defaultFormat;
  }

  private addSpec(transporter: string, specLevel: string, specFormat: string): void {
    const level = this.getLevelOrDefault(specLevel);
    const format = this.getFormatOrDefault(specFormat);

    this.transporters.set(transporter, { level, format });
  }

  private parse(spec: string): void {
    spec
      .split(",")
      .forEach((spec) => {
        const [transporter, level, format] = spec.split(":");

        this.addSpec(transporter, level, format);
      });
  }
}
