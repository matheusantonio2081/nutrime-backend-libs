#!/bin/bash

set -euo pipefail

echo "compiling"
yarn compile

production_build_dir_name=$1

rm -rf "$production_build_dir_name"
mkdir "$production_build_dir_name"

echo "copying lib files to $production_build_dir_name directory"
cp dist/index.d.ts "$production_build_dir_name"
cp dist/index.js "$production_build_dir_name"
cp package.json "$production_build_dir_name"
cp README.md "$production_build_dir_name"
