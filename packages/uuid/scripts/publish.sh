#!/bin/bash

set -e

yarn compile

cp package.json dist
cp README.md dist
rm -rf dist/*.spec.*

cd dist && npm publish
