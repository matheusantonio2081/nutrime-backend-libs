# Uuid

The main difference between this and
[this package on npm](https://www.npmjs.com/package/uuid)
is that it defines a `Uuid` class and can be used to enforce typing.

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/uuid/<version>
```

## Using

Usage example:

```typescript
import { Uuid } from "@nu-libs/uuid";

class User {
  constructor(public readonly id: Uuid) {}

  public isSame(anotherUser: User): boolean {
    return this.id.equals(anotherUser.id);
  }
}

const foo = new User(Uuid.new());
const bar = new User(Uuid.new());

console.log(foo.id.toString()); // 388b932f-0800-412c-a70d-68af2f8e54c8

foo.isSame(bar); // false

new User(Uuid.new()); // This is OK
new User(new Uuid("388b932f-0800-412c-a70d-68af2f8e54c8")); // This is OK
new User(new Uuid("foobar")); // Will throw a TypeError

new User(1); // Can't compile
new User("foobar"); // Can't compile
new User("388b932f-0800-412c-a70d-68af2f8e54c8"); // Can't compile
```
