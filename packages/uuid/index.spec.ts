/* eslint-disable max-lines-per-function,max-nested-callbacks */
import { v4 as uuidv4 } from "uuid";
import { assert } from "chai";

import { Uuid } from "./index";

describe("Uuid", function () {
  describe(".constructor", function () {
    it("Should fails when create an UUID with invalid pattern", function () {
      const identifier = "invalid_uuid";
      assert.throws(() => new Uuid(identifier), "Tried to assign an invalid UUID");
    });
  });

  describe(".equals", function () {
    it("Should be true when compare to Uuid with same identifiers", function () {
      const id1 = Uuid.new();
      const id2 = new Uuid(id1.toString());

      assert.isTrue(id1.equals(id2));
    });
    it("Should be false when compare to Uuid with different identifiers", function () {
      const id1 = Uuid.new();
      const id2 = Uuid.new();

      assert.isFalse(id1.equals(id2));
    });
  });

  describe(".toString", function () {
    it("Should return the identifier of the Uuid", function () {
      const identifier = uuidv4();
      const uuid = new Uuid(identifier);

      assert.equal(uuid.toString(), identifier);
    });
  });
});
/* eslint-enable max-lines-per-function,max-nested-callbacks */
