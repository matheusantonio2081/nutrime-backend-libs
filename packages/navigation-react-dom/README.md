## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/navigation-react-dom/<version>
```

## Using

```typescript
import { NavigationAdapter } from "@nu-libs/navigation";
import { NavigationReactDom } from "@nu-libs/navigation-react-dom";

// example ApplicationContextType expects a navigation that implements NavigationAdapter interface
type ApplicationContextType = {
  navigation: NavigationAdapter;
};

// use NavigationReactDom as navigation implementation
const ApplicationContext = React.createContext<ApplicationContextType>({
  navigation: new NavigationReactDom(),
});
```
