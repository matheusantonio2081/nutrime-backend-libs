import reactRouter from "react-router";
import { assert } from "chai";
import { createSandbox } from "sinon";
import { createMemoryHistory } from "history";

import { NavigationReactDom } from "../index";

describe("ReactRouterDomAdapter", function () {
  const sandbox = createSandbox();

  before(function () {
    sandbox.stub(reactRouter, "useHistory").returns(createMemoryHistory());
  });

  after(function () {
    sandbox.reset();
  });

  describe(".update", function () {
    it("doesNotThrow", async function () {
      const navigationAdapter = new NavigationReactDom();
      assert.doesNotThrow(() => navigationAdapter.update());
    });
  });

  describe(".reset", function () {
    it("reset single route", async function () {
      const navigationAdapter = new NavigationReactDom();

      navigationAdapter.navigate("route-1");
      navigationAdapter.navigate("route-2");

      navigationAdapter.reset("route-0");

      assert.equal("/route-0", navigationAdapter.getCurrentRoute());
    });

    it("reset route with stack", async function () {
      const navigationAdapter = new NavigationReactDom();

      navigationAdapter.navigate("route-4");
      navigationAdapter.navigate("route-3");

      navigationAdapter.reset(["route-1", "route-2"]);
      assert.equal("/route-2", navigationAdapter.getCurrentRoute());

      navigationAdapter.goBack();
      assert.equal("/route-1", navigationAdapter.getCurrentRoute());
    });
  });

  describe(".isScreenFocused", function () {
    it("return true", async function () {
      const navigationAdapter = new NavigationReactDom();
      assert.isTrue(navigationAdapter.isScreenFocused());
    });
  });

  describe(".getParam", function () {
    it("get route state", async function () {
      const navigationAdapter = new NavigationReactDom();

      navigationAdapter.navigate("route-1", {
        state: {
          customState: "some-custom-state",
        },
      });

      const params = navigationAdapter.getParam("customState");

      assert.equal(params, "some-custom-state");
    });
  });

  describe(".goBack", function () {
    it("navigate to previous route", async function () {
      const navigationAdapter = new NavigationReactDom();

      navigationAdapter.navigate("route-1");
      navigationAdapter.navigate("route-2");

      navigationAdapter.goBack();

      assert.equal("/route-1", navigationAdapter.getCurrentRoute());
    });
  });

  describe(".goBackTimes", function () {
    it("navigate 2 routes back", async function () {
      const goBackTimes = 2;
      const navigationAdapter = new NavigationReactDom();

      navigationAdapter.navigate("route-1");
      navigationAdapter.navigate("route-2");
      navigationAdapter.navigate("route-3");
      navigationAdapter.navigate("route-4");

      navigationAdapter.goBackTimes(goBackTimes);

      assert.equal("/route-2", navigationAdapter.getCurrentRoute());
    });
  });

  describe(".getCurrentRoute", function () {
    it("get current location pathname", async function () {
      const navigationAdapter = new NavigationReactDom();

      navigationAdapter.navigate("route-1");
      navigationAdapter.navigate("route-2");

      assert.equal("/route-2", navigationAdapter.getCurrentRoute());
    });
  });
});
