#!/bin/bash

set -euo pipefail

echo "compiling"
yarn compile

production_build_dir_name=$1

rm -rf "$production_build_dir_name"
mkdir -p "$production_build_dir_name"/dist

echo "copying lib files to $production_build_dir_name directory"
cp -R dist/lib "$production_build_dir_name"/dist
cp dist/index.d.ts "$production_build_dir_name"/dist
cp dist/index.js "$production_build_dir_name"/dist
cp package.json "$production_build_dir_name"
cp README.md "$production_build_dir_name"
