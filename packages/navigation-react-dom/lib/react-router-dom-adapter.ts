import * as z from "zod";
import get from "lodash/get";
import { History } from "history";
import { useHistory } from "react-router";
import { NavigationAdapter } from "@nu-libs/navigation";

import { RouteState } from "./route-state";

export class ReactRouterDomAdapter extends NavigationAdapter {
  private history: History<RouteState>;

  public constructor() {
    super();
    this.history = useHistory<RouteState>();
  }

  public update(): void {
    this.history = useHistory();
  }

  public reset(routes: string | string[]): void {
    if (!Array.isArray(routes)) return this.history.replace(routes);

    this.history.replace(routes[0]);

    const missingRoutes = routes.slice(1);

    missingRoutes.forEach((route) => {
      this.history.push(route);
    });
  }

  public isScreenFocused(): boolean {
    return true;
  }

  public navigate(route: string, params?: Record<string, unknown>): void {
    this.history.push(route, { params });
  }

  public getParam(parameterName: string): string {
    const params = this.history.location.state.params;
    const getResult = get(params, `state.${parameterName}`, "");

    return z.string().parse(getResult);
  }

  public goBack(): void {
    this.history.goBack();
  }

  public goBackTimes(count: number): void {
    const back = -1;
    this.history.go(count * back);
  }

  public getCurrentRoute(): string {
    return this.history.location.pathname;
  }
}
