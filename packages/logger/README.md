# Logger

Base logger library with blacklist filter on metadata.

- **This library don't include a "transporter" implementation**
- **This library don't include a "contextStorage" implementation**

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/logger/<version>
```

## Using

Initialize the _logger_ with a transporter implementation:

```typescript
import { Logger, LogRecord, Transporter } from "@nu-libs/logger";

// Create a transporter implementation
Logger.useTransporter({
  log(record: LogRecord): void {
    console.log(record);
  },
});

const logger = Logger.getLogger();
logger.info("Hello", { world: "!" });
```

### Using metadata

#### Adding metadata

This kind of metadata affects the logger globally:

```typescript
Logger.addMetadata("version", "1.0.0");
logger.info("Hello world!");
// name: "default", message: "Hello world!", metadata: { version: "1.0.0" }
```

#### Adding record metadata

This kind of metadata are specific metadata for a single log:

```typescript
logger.error("Request failed", { error: error.message });
// name: "default", message: "Request failed", recordMetadata: { error: "..." }
```

#### Adding context metadata

This logger supports setting contextual data using some
"continuous-local-storage" library.

Be careful: most implementations using this approach have some
performance impact.

Example using [cls-hooked](https://github.com/Jeff-Lewis/cls-hooked)
to add context information with [express](https://expressjs.com/)
middlewares:

```typescript
import express from "express";
import { createNamespace } from "cls-hooked";
import { Logger } from "@nu-libs/logger";

const app = express();
const loggerNamespace = createNamespace("logger");

// the interface should be directly compatible with cls-hooked namespaces
Logger.useContextStorage(loggerNamespace);

app.use((req, res, next) => {
  Logger.runInContext(() => {
    Logger.addContextMetadata("requestIp", req.ip);
    next();
  });
});

app.get("/hello", (req, res) => {
  const logger = Logger.getLogger("my-logger");
  logger.info("Hello world!");
  // name: "my-logger", "message: "Hello world!", contextMetadata: { requestIp: "192.168.0.100" }
  res.status(200).send("world");
});

app.listen(8080);
```

#### Configuring the metadata blacklist/whitelist

This logger support a list of blacklisted and whitelisted keywords to hide
sensitive information from log metadata.

Example:

```typescript
import { Logger } from "@nu-libs/logger";

Logger.setMetadataBlacklist(["pass"]);
Logger.setMetadataWhitelist(["passportId"]);

const logger = Logger.getLogger();

logger.info("User signed in", {
  username: "Fran",
  passportId: "PST-1493",
  password: "secret",
});
// the metadata will be transformed to: {
//   "username": "Fran",
//   "passportId": "PST-1493",
//   "password": "*sensitive*"
// }
```
