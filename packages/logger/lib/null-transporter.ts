/* istanbul ignore file */

import debug from "debug";

import { Transporter } from "./transporter";
import { LogRecord } from "./log-record";

const d = debug("@nu-libs/logger");

export class NullTransporter implements Transporter {
  public log(record: LogRecord): void {
    d(`${record.name}: [${record.level}] ${record.message}`, {
      metadata: record.metadata,
      contextMetadata: record.contextMetadata,
      recordMetadata: record.recordMetadata,
    });
  }
}
