/* istanbul ignore file */

import { ContextStorage } from "./context-storage";

export class NullContextStorage implements ContextStorage {
  public set(_key: string, _value: unknown): void {
    /* nothing to do */
  }

  public run(fn: () => void): void {
    fn();
  }

  public get(): unknown {
    return undefined;
  }
}
