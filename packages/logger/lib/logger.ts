import _ from "lodash";
import debug from "debug";

import { ContextStorage } from "./context-storage";
import { LogLevel } from "./log-level";
import { MetadataFilter } from "./metadata-filter";
import { NullContextStorage } from "./null-context-storage";
import { NullTransporter } from "./null-transporter";
import { Transporter } from "./transporter";

const d = debug("@nu-libs/logger");

export class Logger {
  private static readonly metadata: Record<string, unknown> = {};

  private static readonly contextMetadataKeys: string[] = [];

  private static metadataFilter = new MetadataFilter([], []);

  private static context: ContextStorage = new NullContextStorage();

  private static transporter: Transporter = new NullTransporter();

  public constructor(private readonly name: string) {}

  public static getLogger(
    className: string | { name: string } = "default"
  ): Logger {
    return new Logger(
      typeof className === "string" ? className : className.name
    );
  }

  public static setMetadataBlacklist(keys: string[]): void {
    this.metadataFilter = new MetadataFilter(
      keys,
      this.metadataFilter.whitelistedKeys
    );
  }

  public static setMetadataWhitelist(keys: string[]): void {
    this.metadataFilter = new MetadataFilter(
      this.metadataFilter.blacklistedKeys,
      keys
    );
  }

  public static useTransporter(transporter: Transporter): void {
    this.transporter = transporter;
  }

  public static useContextStorage(storage: ContextStorage): void {
    this.context = storage;
  }

  public static runInContext(fn: () => void): void {
    this.context.run(fn);
  }

  public static addMetadata(key: string, value: unknown): void {
    this.metadata[key] = value;
  }

  public static addContextMetadata(key: string, value: unknown): void {
    if (!this.contextMetadataKeys.some((x) => x === key)) {
      this.contextMetadataKeys.push(key);
    }

    this.context.set(key, value);
  }

  public info(message: string, metadata: Record<string, unknown> = {}): void {
    this.log(LogLevel.info, message, metadata);
  }

  public debug(message: string, metadata: Record<string, unknown> = {}): void {
    this.log(LogLevel.debug, message, metadata);
  }

  public error(message: string, metadata: Record<string, unknown> = {}): void {
    this.log(LogLevel.error, message, metadata);
  }

  public log(
    level: LogLevel,
    message: string,
    metadata: Record<string, unknown> = {}
  ): void {
    try {
      Logger.transporter.log({
        name: this.name,
        level,
        timestamp: Date.now(),
        message,
        metadata: Logger.metadata,
        contextMetadata: Logger.metadataFilter.filter(
          this.getContextMetadata()
        ),
        recordMetadata: Logger.metadataFilter.filter(metadata),
      });
    } catch (error: unknown) {
      d("Error", error, message);
    }
  }

  private getContextMetadata(): Record<string, unknown> {
    const metadata: Record<string, unknown> = {};

    Logger.contextMetadataKeys.forEach((key) => {
      const value = Logger.context.get(key);

      if (!_.isNil(value)) {
        metadata[key] = value;
      }
    });

    return metadata;
  }
}
