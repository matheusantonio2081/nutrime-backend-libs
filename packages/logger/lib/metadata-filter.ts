import _ from "lodash";

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
export class MetadataFilter {
  private readonly jsPrimitives = ["string", "number", "boolean", "bigint", "symbol"];

  private readonly placeholder = "*sensitive*";

  public constructor(
    public readonly blacklistedKeys: string[] = [],
    public readonly whitelistedKeys: string[] = [],
  ) {}

  // eslint-disable-next-line max-lines-per-function
  public filter(record: { [key: string]: any }): { [key: string]: unknown } {
    const clonedRecord = _.cloneWith(record);

    // eslint-disable-next-line complexity,max-lines-per-function,max-statements
    Object.keys(clonedRecord).forEach((key) => {
      if (this.isOnBlacklist(key) && this.isArrayWithPrimitives(clonedRecord[key])) {
        clonedRecord[key] = clonedRecord[key].map((value: unknown) => this.setPlaceholder(value));
        return;
      }

      if (this.isPlainObject(clonedRecord[key])) {
        clonedRecord[key] = this.filter(clonedRecord[key]);
        return;
      }

      if (this.isJSONString(clonedRecord[key])) {
        clonedRecord[key] = JSON.stringify(this.filter(JSON.parse(clonedRecord[key])));
        return;
      }

      if (!this.isJSONType(clonedRecord[key])) {
        // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
        delete clonedRecord[key];
        return;
      }

      if (this.isOnBlacklist(key)) {
        clonedRecord[key] = this.setPlaceholder(clonedRecord[key]);
      }
    });

    return clonedRecord;
  }

  /* eslint-disable @typescript-eslint/no-magic-numbers */
  private setPlaceholder(value: unknown): string {
    if (typeof value !== "string") {
      return this.placeholder;
    }

    if (value.length < 8) {
      return this.placeholder;
    }

    return `${value.slice(0, 2)}${this.placeholder}${value.slice(-2)}`;
  }
  /* eslint-enable @typescript-eslint/no-magic-numbers */

  private isOnBlacklist(key: string): boolean {
    if (this.blacklistedKeys.some((blacklistedKey) => key.toLowerCase().includes(blacklistedKey.toLowerCase()))) {
      return !this.whitelistedKeys.some((whitelistKey) => key.toLowerCase() === whitelistKey.toLowerCase());
    }

    return false;
  }

  private isPlainObject(value: any): boolean {
    // The logger will accept only "plain" objects like: { foo: 123 } and Arrays
    return value?.constructor === Object || Array.isArray(value);
  }

  private isArrayWithPrimitives(value: unknown): value is unknown[] {
    return Array.isArray(value) && value.some((element) => this.jsPrimitives.includes(typeof element));
  }

  // eslint-disable-next-line complexity
  private isJSONType(value: unknown): boolean {
    if (value === undefined) {
      return false;
    }

    return typeof value !== "object" || Array.isArray(value) || this.isPlainObject(value) || value instanceof Date;
  }

  // eslint-disable-next-line complexity
  private isJSONString(value: unknown): boolean {
    if (typeof value !== "string") {
      return false;
    }

    if (!value.startsWith("{") && !value.startsWith("[")) {
      return false;
    }

    try {
      JSON.parse(value);
      return true;
    } catch (e: unknown) {
      return false;
    }
  }
}
/* eslint-enable @typescript-eslint/no-unsafe-assignment */
/* eslint-enable @typescript-eslint/no-explicit-any */
/* eslint-enable @typescript-eslint/no-unsafe-member-access */
