export enum LogLevel {
  info = "info",
  debug = "debug",
  error = "error",
}
