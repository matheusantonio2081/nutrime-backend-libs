import faker from "faker";
import { assert } from "chai";

import { MetadataFilter } from "../lib/metadata-filter";
import { Dummy } from "./dummy";

const placeholder = "*sensitive*";
const loggerMetadataFilter = new MetadataFilter(["card", "pass"], ["passportId"]);
const runFilterAndExpectations = ({ value, expected }: {
  value: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
  expected: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
}): void => {
  assert.deepEqual(loggerMetadataFilter.filter(value), expected);
};

describe("MetadataFilter", function () {
  describe(".filter", function () {
    it("should replace string values from list with placeholders", function () {
      runFilterAndExpectations({
        value: { cardNumber: "5175033959689000" },
        expected: { cardNumber: `51${placeholder}00` },
      });
    });

    it("should replace non string values from list with placeholders", function () {
      runFilterAndExpectations({
        value: { cardNumber: 1234567890 },
        expected: { cardNumber: placeholder },
      });
    });

    it("should not replace values with placeholder when not listed", function () {
      runFilterAndExpectations({
        value: { name: "John Doe" },
        expected: { name: "John Doe" },
      });
    });

    it("should deeply replace values from list with placeholders", function () {
      runFilterAndExpectations({
        value: {
          cardInfo: { cardNumber: "0000000000000000" },
        },
        expected: {
          cardInfo: { cardNumber: `00${placeholder}00` },
        },
      });
    });

    it("should remove any object that are not directly constructed from Object class", function () {
      runFilterAndExpectations({
        value: { test: new Dummy() },
        expected: {},
      });
    });

    it("should deeply remove any object that are not directly constructed from Object class", function () {
      runFilterAndExpectations({
        value: {
          deep: { test: new Dummy() },
        },
        expected: {
          deep: {},
        },
      });
    });

    it("should inspect JSON string for blacklisted keys", function () {
      runFilterAndExpectations({
        value: { card: "{\"cardNumber\":\"000000000000\",\"holder\":\"FOO BAR\"}" },
        expected: { card: `{"cardNumber":"00${placeholder}00","holder":"FOO BAR"}` },
      });
    });

    it("should ignore blacklisting a broken JSON string", function () {
      runFilterAndExpectations({
        value: { list: "[{\"cardNumber\":\"000000000000\",\"holder\":\"FOO BAR\"" },
        expected: { list: "[{\"cardNumber\":\"000000000000\",\"holder\":\"FOO BAR\"" },
      });
    });

    it("should inspect array elements for blacklisted keys", function () {
      runFilterAndExpectations({
        value: { list: [{ cardNumber: "000000000000" }] },
        expected: { list: [{ cardNumber: `00${placeholder}00` }] },
      });
    });

    it("should inspect JSON stringified array elements for blacklisted keys", function () {
      runFilterAndExpectations({
        value: { cards: "[{\"cardNumber\":\"000000000000\"}]" },
        expected: { cards: `[{"cardNumber":"00${placeholder}00"}]` },
      });
    });

    it("should replace array with primitive elements of a blacklisted key", function () {
      const values = [
        faker.datatype.boolean(),
        faker.datatype.string(2),
        faker.datatype.number(),
      ];
      const allowedValue = faker.random.arrayElement(values);
      runFilterAndExpectations({
        value: {
          cards: [faker.random.arrayElement(values), faker.random.arrayElement(values)],
          brand: [allowedValue],
        },
        expected: {
          cards: [placeholder, placeholder],
          brand: [allowedValue],
        },
      });
    });

    it("should inspect JSON stringified array with primitive elements for blacklisted keys", function () {
      runFilterAndExpectations({
        value: { cards: "[{\"cardNumbers\":[\"111111111111\",\"999999999999\"]}]" },
        expected: { cards: `[{"cardNumbers":["11${placeholder}11","99${placeholder}99"]}]` },
      });
    });

    it("should return a cloned instance of the object", function () {
      const value = {};
      assert.notStrictEqual(loggerMetadataFilter.filter(value), value);
    });

    it("should deeply return a cloned instance of the object", function () {
      const value = { deep: {} };
      assert.notStrictEqual(loggerMetadataFilter.filter(value.deep), value.deep);
    });

    it("should remove null or undefined values", function () {
      runFilterAndExpectations({
        value: {
          age: null,
          job: undefined,
        },
        expected: {},
      });
    });

    it("should deeply remove null or undefined values", function () {
      runFilterAndExpectations({
        value: {
          person: {
            age: null,
            job: undefined,
          },
        },
        expected: {
          person: {},
        },
      });
    });

    it("should allow keywords on whitelist", function () {
      runFilterAndExpectations({
        value: {
          user: {
            password: "123456789",
            passportId: "PASS-0148",
          },
        },
        expected: {
          user: {
            password: `12${placeholder}89`,
            passportId: "PASS-0148",
          },
        },
      });
    });
  });
});
