import { Transporter, LogRecord } from "../index";

export class TestTransporter implements Transporter {
  private logs: LogRecord[] = [];

  private forceError = false;

  public log(record: LogRecord): void {
    if (this.forceError) {
      throw new Error("Transporter error");
    }

    this.logs.push(record);
  }

  public getLatest(): LogRecord | undefined {
    return this.logs[this.logs.length - 1];
  }

  public forcingError(): void {
    this.forceError = true;
  }

  public clean(): void {
    this.logs = [];
    this.forceError = false;
  }
}
