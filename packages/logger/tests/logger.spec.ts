import faker from "faker";
import { assert } from "chai";

import { Logger, LogLevel } from "../index";
import { Dummy } from "./dummy";
import { TestContextStorage } from "./test-context-storage";
import { TestTransporter } from "./test-transporter";

const transporter = new TestTransporter();
const contextStorage = new TestContextStorage();

Logger.useContextStorage(contextStorage);
Logger.useTransporter(transporter);

describe("Logger", function () {
  afterEach(function () {
    transporter.clean();
    contextStorage.clean();
  });

  describe(".getLogger", function () {
    it("should use the default name", function () {
      const logger = Logger.getLogger();
      logger.info(faker.lorem.text());
      assert.equal(transporter.getLatest()?.name, "default");
    });

    it("should use a specific name", function () {
      const name = faker.lorem.word();
      const logger = Logger.getLogger(name);
      logger.info(faker.lorem.text());
      assert.equal(transporter.getLatest()?.name, name);
    });

    it("should use a class name", function () {
      const logger = Logger.getLogger(Dummy);
      logger.info(faker.lorem.text());
      assert.equal(transporter.getLatest()?.name, Dummy.name);
    });
  });

  describe(".setMetadataBlacklist", function () {
    const blacklistKeyword = faker.random.word();
    const logger = Logger.getLogger();

    it("should initialize without adding the keyword on blacklist", function () {
      const value = faker.random.alphaNumeric(4);
      Logger.addContextMetadata(blacklistKeyword, value);
      logger.info(faker.lorem.text(), { [blacklistKeyword]: value });
      assert.include(transporter.getLatest()?.contextMetadata, { [blacklistKeyword]: value });
      assert.include(transporter.getLatest()?.recordMetadata, { [blacklistKeyword]: value });
    });

    it("should add the keyword on blacklist", function () {
      Logger.setMetadataBlacklist([blacklistKeyword]);
      const value = faker.random.alphaNumeric(4);
      Logger.addContextMetadata(blacklistKeyword, value);
      logger.info(faker.lorem.text(), { [blacklistKeyword]: value });
      assert.include(transporter.getLatest()?.contextMetadata, { [blacklistKeyword]: "*sensitive*" });
      assert.include(transporter.getLatest()?.recordMetadata, { [blacklistKeyword]: "*sensitive*" });
      Logger.setMetadataBlacklist([]);
    });
  });

  describe(".setMetadataWhitelist", function () {
    const keyword = faker.random.word();
    const logger = Logger.getLogger();

    it("should add the keyword on whitelist", function () {
      Logger.setMetadataBlacklist([keyword]);
      Logger.setMetadataWhitelist([keyword]);
      const value = faker.random.alphaNumeric(4);
      Logger.addContextMetadata(keyword, value);
      logger.info(faker.lorem.text(), { [keyword]: value });
    });
  });

  describe(".addMetadata", function () {
    const key = faker.random.word();
    const logger = Logger.getLogger();

    it("should add the metadata", function () {
      const value: unknown = faker.random.arrayElement([
        faker.datatype.string(),
        faker.datatype.number(),
        faker.datatype.json(),
      ]);
      Logger.addMetadata(key, value);
      logger.info(faker.lorem.text());
      assert.include(transporter.getLatest()?.metadata, { [key]: value });
    });
  });

  describe(".addContextMetadata", function () {
    const key = faker.random.word();
    const logger = Logger.getLogger();

    it("should add the context metadata", function () {
      const value: unknown = faker.random.arrayElement([
        faker.datatype.string(),
        faker.datatype.number(),
        faker.datatype.json(),
      ]);
      Logger.addContextMetadata(key, value);
      logger.info(faker.lorem.text());
      assert.include(transporter.getLatest()?.contextMetadata, { [key]: value });
    });

    it("should reset the context metadata", function () {
      Logger.addContextMetadata(key, undefined);
      logger.info(faker.lorem.text());
      assert.doesNotHaveAnyKeys(transporter.getLatest()?.contextMetadata, [key]);
    });
  });

  describe(".runInContext", function () {
    it("should run the context fn", function () {
      let contextFnCalled = false;
      const fn = () => {
        contextFnCalled = true;
      };
      Logger.runInContext(fn);
      assert.isTrue(contextFnCalled);
      assert.equal(contextStorage.contextFn, fn);
    });
  });

  describe(".log", function () {
    const logger = Logger.getLogger();
    const message = faker.random.words();

    it("should log using the specified level", function () {
      const level = faker.random.arrayElement([LogLevel.info, LogLevel.debug, LogLevel.error]);
      logger.log(level, message);
      assert.equal(transporter.getLatest()?.level, level);
      assert.equal(transporter.getLatest()?.message, message);
    });

    it("should log with info level", function () {
      logger.info(message);
      assert.equal(transporter.getLatest()?.level, LogLevel.info);
      assert.equal(transporter.getLatest()?.message, message);
    });

    it("should log with debug level", function () {
      logger.debug(message);
      assert.equal(transporter.getLatest()?.level, LogLevel.debug);
      assert.equal(transporter.getLatest()?.message, message);
    });

    it("should log with error level", function () {
      logger.error(message);
      assert.equal(transporter.getLatest()?.level, LogLevel.error);
      assert.equal(transporter.getLatest()?.message, message);
    });

    it("shouldn't throw when the transporter has an error", function () {
      transporter.forcingError();
      assert.doesNotThrow(() => logger.info(message));
    });
  });
});
