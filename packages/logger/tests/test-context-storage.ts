import { ContextStorage } from "../lib/context-storage";

export class TestContextStorage implements ContextStorage {
  public contextFn?: () => void;

  private storage: Record<string, string | undefined> = {};

  public get(key: string): string | undefined {
    return this.storage[key];
  }

  public run(fn: () => void): void {
    this.contextFn = fn;
    fn();
  }

  public set(key: string, value?: string): void {
    this.storage[key] = value;
  }

  public clean(): void {
    this.storage = {};
  }
}
