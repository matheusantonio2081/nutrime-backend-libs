# Secret Manager

Library to get the secrets from the AWS Secrets Manager and handle caching the
secrets in memory.

## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/secret-manager/<version>
```

## Using

```typescript
import { SecretManager, NullClient, AWSClient } from "@nu-libs/secret-manager";

// Configure your secret manager client
SecretManager.useClient(new NullClient()); // default
SecretManager.useClient(new AWSClient());

const secretManager = new SecretManager({
  cacheTTLInSeconds: 3600, // Optional. Default value: 3600
});

// Fetch a value from SecretManager
const username = await secretManager.get("production/database", "username");
const password = await secretManager.get("production/database", "password");

// Fetch a value from SecretManager or use a default value when not exists
const username = await secretManager.getOrDefault(
  "production/database",
  "username",
  () => process.env.DB_USERNAME
);
const password = await secretManager.getOrDefault(
  "production/database",
  "password",
  () => process.env.DB_PASSWORD
);
```
