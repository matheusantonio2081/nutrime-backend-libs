export { SecretManager } from "./lib/secret-manager";
export { SecretManagerOptions } from "./lib/secret-manager-options";
export { Client } from "./lib/clients/client";
export { AWSClient } from "./lib/clients/aws-client";
export { NullClient } from "./lib/clients/null-client";
