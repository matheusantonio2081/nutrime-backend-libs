export interface Client {
  getSecret(secretName: string): Promise<string | undefined>;
  deserializeSecretValue(secretValue: string, key: string): Promise<string | undefined>;
}
