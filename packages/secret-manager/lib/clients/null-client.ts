import { Client } from "./client";

export class NullClient implements Client {
  public async getSecret(_secretName: string): Promise<string | undefined> {
    return undefined;
  }

  public async deserializeSecretValue(_secretValue: string, _key: string): Promise<string | undefined> {
    return undefined;
  }
}
