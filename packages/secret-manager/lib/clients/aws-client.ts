import AWS from "aws-sdk";
import _ from "lodash";
import debug from "debug";

import { Client } from "./client";

const d = debug("@nu-libs/secret-manager");

export class AWSClient implements Client {
  private readonly secretsManager: AWS.SecretsManager;

  public constructor(secretsManager?: AWS.SecretsManager) {
    this.secretsManager = secretsManager ?? new AWS.SecretsManager();
  }

  public async getSecret(secretName: string): Promise<string | undefined> {
    try {
      const response = await this.secretsManager
        // eslint-disable-next-line @typescript-eslint/naming-convention
        .getSecretValue({ SecretId: secretName })
        .promise();

      return response.SecretString;
    } catch (error: unknown) {
      d("Error getting secret", error, secretName);
      return undefined;
    }
  }

  public async deserializeSecretValue(
    secretValue: string,
    key: string
  ): Promise<string | undefined> {
    try {
      const json: unknown = JSON.parse(secretValue);
      const value: unknown = _.get(json, key);

      return this.parseValueToString(value);
    } catch (error: unknown) {
      d("Error deserializing secret", error, key);
      return undefined;
    }
  }

  private parseValueToString(value: unknown): string | undefined {
    if (typeof value === "string") {
      return value;
    }

    if (this.isPrimitiveValue(value)) {
      return JSON.stringify(value);
    }

    return undefined;
  }

  private isPrimitiveValue(value: unknown): boolean {
    const primitives = ["string", "number", "boolean"];
    return primitives.includes(typeof value);
  }
}
