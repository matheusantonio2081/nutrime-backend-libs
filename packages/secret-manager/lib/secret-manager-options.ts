export interface SecretManagerOptions {
  cacheTTLInSeconds?: number;
}
