import NodeCache from "node-cache";

export class Cache {
  private static instance?: Cache;

  private readonly cache: NodeCache;

  private constructor() {
    this.cache = new NodeCache({
      useClones: false,
      deleteOnExpire: true,
    });
  }

  public static getInstance(): Cache {
    if (this.instance === undefined) {
      this.instance = new Cache();
    }

    return this.instance;
  }

  public get(key: string): string | undefined {
    return this.cache.get(key);
  }

  public set(key: string, value: string, cacheTTLInSeconds: number): void {
    this.cache.set(key, value, cacheTTLInSeconds);
  }
}
