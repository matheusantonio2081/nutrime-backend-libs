import { Cache } from "./cache";
import { Client } from "./clients/client";
import { NullClient } from "./clients/null-client";
import { SecretManagerOptions } from "./secret-manager-options";

export class SecretManager {
  private static client: Client = new NullClient();

  private readonly cache = Cache.getInstance();

  private readonly cacheTTLInSeconds: number = 3600;

  public constructor(options: SecretManagerOptions = {}) {
    if (options.cacheTTLInSeconds !== undefined) {
      this.cacheTTLInSeconds = options.cacheTTLInSeconds;
    }
  }

  public static useClient(client: Client): void {
    this.client = client;
  }

  public async get(secretName: string, key: string): Promise<string | undefined> {
    let secret = this.cache.get(secretName);

    if (secret === undefined) {
      secret = await SecretManager.client.getSecret(secretName);

      if (secret === undefined) {
        return undefined;
      }

      this.cache.set(secretName, secret, this.cacheTTLInSeconds);
    }

    return SecretManager.client.deserializeSecretValue(secret, key);
  }

  public async getOrDefault(
    secretName: string,
    key: string,
    defaultFn: () => Promise<string> | string
  ): Promise<string> {
    return await this.get(secretName, key) ?? await defaultFn();
  }
}
