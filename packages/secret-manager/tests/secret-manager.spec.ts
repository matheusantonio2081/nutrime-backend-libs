import { expect } from "chai";
import faker from "faker";

import { SecretManager } from "../index";
import { TestClient } from "./test-client";

const sleep = async (time: number): Promise<void> => new Promise((resolve) => {
  setTimeout(() => resolve(), time);
});

const testClient = new TestClient();

SecretManager.useClient(testClient);

describe("SecretManager", function () {
  afterEach(() => {
    testClient.cleanSecrets();
  });

  describe(".get", function () {
    it("should return value from secrets manager", async function () {
      // given
      const secretName = faker.datatype.string();
      const secretKey = faker.datatype.string();
      const secretValue = faker.datatype.string();
      const secretManager = new SecretManager();

      // when
      testClient.setSecretValue(secretName, secretKey, secretValue);
      const returnedSecretValue = await secretManager.get(secretName, secretKey);

      // then
      expect(returnedSecretValue).to.be.eq(secretValue);
    });

    it("should return value from cache", async function () {
      // given
      const secretName = "test-cached-key";
      const secretKey = faker.datatype.string();
      const secretManager = new SecretManager();

      // when
      testClient.setSecretValue(secretName, secretKey, "secret1");
      const returnedSecretValue = await secretManager.get(secretName, secretKey);
      testClient.setSecretValue(secretName, secretKey, "secret2");
      const cachedReturnedValue = await secretManager.get(secretName, secretKey);

      // then
      expect(returnedSecretValue).to.be.eq("secret1");
      expect(cachedReturnedValue).to.be.eq("secret1");
    });

    it("should return from secret manager after cache expired", async function () {
      // given
      const secretName = "test-expired-cached-key";
      const secretKey = faker.datatype.string();
      const secretManager = new SecretManager({
        cacheTTLInSeconds: 1,
      });

      // when
      testClient.setSecretValue(secretName, secretKey, "secret1");
      const returnedSecretValue = await secretManager.get(secretName, secretKey);
      testClient.setSecretValue(secretName, secretKey, "secret2");
      await sleep(1100);
      const cachedReturnedValue = await secretManager.get(secretName, secretKey);

      // then
      expect(returnedSecretValue).to.be.eq("secret1");
      expect(cachedReturnedValue).to.be.eq("secret2");
    });

    it("should return undefined when secret not exists", async function () {
      const secretManager = new SecretManager();
      const secretValue = await secretManager.get(faker.datatype.string(), faker.datatype.string());

      expect(secretValue).to.be.eq(undefined);
    });

    it("should return undefined when secret value not exists", async function () {
      const secretName = faker.datatype.string();
      const secretManager = new SecretManager();

      testClient.setSecret(secretName, {});
      const secretValue = await secretManager.get(secretName, faker.datatype.string());

      expect(secretValue).to.be.eq(undefined);
    });
  });

  describe(".getOrDefault", function () {
    it("should return the secret value when it exists", async function () {
      // given
      const secretName = faker.datatype.string();
      const secretKey = faker.datatype.string();
      const secretValue = faker.datatype.string();
      const secretManager = new SecretManager();

      // when
      testClient.setSecretValue(secretName, secretKey, secretValue);
      const returnedSecretValue = await secretManager.getOrDefault(
        secretName,
        secretKey,
        () => faker.datatype.string()
      );

      // then
      expect(returnedSecretValue).to.be.eq(secretValue);
    });

    it("should return the default value when secret not exists", async function () {
      const secretName = faker.datatype.string();
      const defaultValue = faker.datatype.string();
      const secretManager = new SecretManager();
      testClient.setSecret(secretName, {});
      const secretValue = await secretManager.getOrDefault(
        secretName,
        faker.datatype.string(),
        async () => defaultValue
      );

      expect(secretValue).to.be.eq(defaultValue);
    });
  });
});
