import faker from "faker";
import { expect } from "chai";

import { AWSClient } from "../index";

const secretManager = new AWSClient();

describe("AWSClient", function () {
  const someString = faker.datatype.string();
  const someNumber = faker.datatype.number();
  const someBoolean = faker.datatype.boolean();
  const secret = JSON.stringify({
    someString,
    someNumber,
    someBoolean,
    someObject: faker.datatype.array(),
  });

  describe(".deserializeSecretValue", function () {
    it("Should return undefined when key isn't in json", async function () {
      const value = await secretManager.deserializeSecretValue(
        secret,
        "whatever"
      );

      expect(value).to.be.eq(undefined);
    });

    it("Should return string value when key is in json", async function () {
      const value = await secretManager.deserializeSecretValue(secret, "someString");
      expect(value).to.be.eq(someString);
    });

    it("Should return numeric value when key is in json", async function () {
      const value = await secretManager.deserializeSecretValue(secret, "someNumber");
      expect(value).to.be.eq(String(someNumber));
    });

    it("Should return boolean value when key is in json", async function () {
      const value = await secretManager.deserializeSecretValue(secret, "someBoolean");
      expect(value).to.be.eq(String(someBoolean));
    });

    it("Should return undefined when key isn't a primitive (string, number or boolean)", async function () {
      const value = await secretManager.deserializeSecretValue(secret, "someObject");
      expect(value).to.be.eq(undefined);
    });
  });
});
