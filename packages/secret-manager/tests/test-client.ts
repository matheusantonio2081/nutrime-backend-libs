import _ from "lodash";

import { Client } from "../index";

export class TestClient implements Client {
  public constructor(private secrets: Record<string, Record<string, unknown> | undefined> = {}) {}

  public async getSecret(secretName: string): Promise<string | undefined> {
    const secret = this.secrets[secretName];

    if (secret === undefined) {
      return undefined;
    }

    return JSON.stringify(secret);
  }

  public async deserializeSecretValue(secretValue: string, key: string): Promise<string | undefined> {
    try {
      const json: unknown = JSON.parse(secretValue);
      const value: unknown = _.get(json, key);

      return this.parseValueToString(value);
    } catch (error: unknown) {
      return undefined;
    }
  }

  public setSecret(secretName: string, secret: Record<string, unknown>): void {
    this.secrets[secretName] = secret;
  }

  public setSecretValue(secretName: string, key: string, value: unknown): void {
    this.secrets[secretName] = Object.assign({}, this.secrets[secretName], { [key]: value });
  }

  public cleanSecrets(): void {
    this.secrets = {};
  }

  private parseValueToString(value: unknown): string | undefined {
    if (typeof value === "string") {
      return value;
    }

    if (this.isPrimitiveValue(value)) {
      return JSON.stringify(value);
    }

    return undefined;
  }

  private isPrimitiveValue(value: unknown): boolean {
    const primitives = ["string", "number", "boolean"];
    return primitives.includes(typeof value);
  }
}
