## Install

```
yarn add git+ssh://git@gitlab.com:matheusantonio2081/nutrime-backend-libs.git#packages/navigation-react-native/<version>
```

## Using

```typescript
import { NavigationAdapter } from "@nu-libs/navigation";
import { NavigationReactNative } from "@nu-libs/navigation-react-native";

// example ApplicationContextType expects a navigation that implements NavigationAdapter interface
type ApplicationContextType = {
  navigation: NavigationAdapter;
};

// use NavigationReactNative as navigation implementation
const ApplicationContext = React.createContext<ApplicationContextType>({
  navigation: new NavigationReactNative(),
});
```
