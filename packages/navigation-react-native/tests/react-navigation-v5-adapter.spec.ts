import proxyquire from "proxyquire";
import { assert } from "chai";
import { spy } from "sinon";

import { ReactNavigationV5Adapter } from "../lib/react-navigation-v5-adapter";

function mockNavigationNative(
  useRoute?: Record<string, unknown>,
  useNavigation?: Record<string, unknown>
): ReactNavigationV5Adapter {
  const NavigationReactNative = proxyquire(
    "../lib/react-navigation-v5-adapter.js",
    {
      "@react-navigation/native": {
        "@noCallThru": true,
        useRoute: () => ({
          name: "",
          ...useRoute,
        }),
        useNavigation: () => ({
          goBack: spy(),
          pop: spy(),
          navigate: spy(),
          reset: spy(),
          ...useNavigation,
        }),
        useNavigationState: () => ({
          name: "",
        }),
      },
    }
  );

  return new NavigationReactNative.ReactNavigationV5Adapter();
}

describe("NavigationReactNative", function () {
  describe(".update", function () {
    it("should not throw when calling method", function () {
      const reactNavigationV5Adapter = mockNavigationNative();
      assert.doesNotThrow(() => reactNavigationV5Adapter.update());
    });
  });

  describe(".reset", function () {
    it("can reset single or multiple routes", function () {
      const resetSpy = spy();
      const reactNavigationV5Adapter = mockNavigationNative(
        {},
        {
          reset: resetSpy,
        }
      );

      assert.doesNotThrow(() => reactNavigationV5Adapter.reset(""));
      assert.doesNotThrow(() => reactNavigationV5Adapter.reset(["route"]));
      assert.isTrue(resetSpy.calledTwice);
    });
  });

  describe(".isScreenFocused", function () {
    it("should return true when route is focused", function () {
      const reactNavigationV5Adapter = mockNavigationNative();
      assert.isTrue(reactNavigationV5Adapter.isScreenFocused());
    });
  });

  describe(".navigate", function () {
    it("should navigate to the given route", function () {
      const navigateSpy = spy();
      const reactNavigationV5Adapter = mockNavigationNative(
        {},
        {
          navigate: navigateSpy,
        }
      );

      reactNavigationV5Adapter.navigate("route");

      assert.isTrue(navigateSpy.calledOnceWith("route"));
    });
  });

  describe(".getParam", function () {
    it("should return param by given key", function () {
      const reactNavigationV5Adapter = mockNavigationNative({
        params: {
          foo: "bar",
        },
      });

      assert.equal(reactNavigationV5Adapter.getParam("foo"), "bar");
    });

    it("return undefined when param key does not exists", function () {
      const reactNavigationV5Adapter = mockNavigationNative();

      assert.doesNotThrow(() => reactNavigationV5Adapter.getParam("foo"));
      assert.isUndefined(reactNavigationV5Adapter.getParam("foo"));
    });
  });

  describe(".goBack", function () {
    it("should return to the previous route", function () {
      const goBackSpy = spy();
      const reactNavigationV5Adapter = mockNavigationNative(
        {},
        {
          goBack: goBackSpy,
        }
      );

      reactNavigationV5Adapter.goBack();

      assert.isTrue(goBackSpy.calledOnce);
    });
  });

  describe(".goBackTimes", function () {
    it("should navigate 2 routes back", function () {
      const popSpy = spy();
      const reactNavigationV5Adapter = mockNavigationNative(
        {},
        {
          pop: popSpy,
        }
      );

      reactNavigationV5Adapter.goBackTimes(2);

      assert.isTrue(popSpy.calledOnceWith(2));
    });
  });

  describe(".getCurrentRoute", function () {
    it("should get name of the current route", function () {
      const reactNavigationV5Adapter = mockNavigationNative({
        name: "some-route",
      });

      assert.equal(reactNavigationV5Adapter.getCurrentRoute(), "some-route");
    });
  });
});
