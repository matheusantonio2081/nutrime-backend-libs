import last from "lodash/last";
import {
  useNavigation,
  useRoute,
  useNavigationState,
  RouteProp,
  ParamListBase,
} from "@react-navigation/native";

import { NavigationAdapter } from "@nu-libs/navigation";

type ResetObject = {
  index: number;
  routes: Array<{ name: string }>;
};

type RouteType = RouteProp<ParamListBase, string>;

export class ReactNavigationV5Adapter extends NavigationAdapter {
  private navigation: any;
  private navigationRoute: any;
  private activeRoute: any;

  public constructor() {
    super();
    this.navigation = useNavigation();
    this.navigationRoute = useRoute<RouteType>();
    this.activeRoute = useNavigationState((state) => last(state.routes));
  }

  private createResetObject(routes: string | string[]): ResetObject {
    if (Array.isArray(routes)) {
      return {
        index: routes.length - 1,
        routes: routes.map((routeName) => ({
          name: routeName,
        })),
      };
    }

    return {
      index: 0,
      routes: [{ name: routes }],
    };
  }

  public update(): void {
    this.navigation = useNavigation();
    this.navigationRoute = useRoute<RouteType>();
    this.activeRoute = useNavigationState((state) => last(state.routes));
  }

  public reset(routes: string | string[]): void {
    this.navigation.reset(this.createResetObject(routes));
  }

  public isScreenFocused(): boolean {
    return this.navigationRoute.name === this.activeRoute!.name;
  }

  public navigate(route: string, params?: Record<string, string>): void {
    this.navigation.navigate(route, params);
  }

  public getParam(parameterName: string): any {
    if (!this.navigationRoute || !this.navigationRoute.params) return;

    return this.navigationRoute.params[parameterName];
  }

  public goBack(): void {
    this.navigation.goBack();
  }

  public goBackTimes(count: number): void {
    this.navigation.pop(count);
  }

  public getCurrentRoute(): string {
    return this.navigationRoute.name;
  }
}
