docker-build-%:
	docker-compose -f docker-compose-$*.yml build

install-%:
	docker-compose -f docker-compose-$*.yml run --rm app ./scripts/install-$*.sh $(GITHUB_TOKEN)

shell:
	docker-compose -f docker-compose-development.yml run --rm -w $(PWD)/packages app bash

patch-versions:
	docker-compose -f docker-compose-development.yml run --rm app ./scripts/patch-versions.js

print-versions:
	docker-compose -f docker-compose-development.yml run --rm app ./scripts/print-versions.sh

lint-%:
	docker run --rm -v $(PWD):/mnt koalaman/shellcheck:stable ./**/*.sh
	docker-compose -f docker-compose-$*.yml run --rm app ./scripts/run-all-linters.sh

test-%:
	docker-compose -f docker-compose-$*.yml run --rm app ./scripts/run-all-tests.sh

compile-%:
	docker-compose -f docker-compose-$*.yml run --rm app ./scripts/compile-all.sh

git-clean-%:
	docker-compose -f docker-compose-$*.yml run --rm app git clean -fX .

create-production-builds:
	docker-compose -f docker-compose-production.yml run --rm app ./scripts/build-production-all.sh

generate-tag:
	docker-compose -f docker-compose-production.yml run --rm app ./scripts/generate-tag-all.sh $(GITHUB_TOKEN)

generate-tag-local:
	docker-compose -f docker-compose-development.yml run --rm app ./scripts/generate-tag-all-local.sh

publish:
	docker-compose -f docker-compose-production.yml run --rm app ./scripts/publish-all.sh


