#!/bin/bash

sources="$(find packages -mindepth 1 -maxdepth 1 -type d -printf "%p," | sed 's/,$//')"

exclusions='**/node_modules/**'
exclusions="$exclusions,$(find packages -mindepth 1 -maxdepth 2 -type f -name .sonar-exclude -printf "%h/**," | sed 's/,$//')"

test_inclusions='packages/**/*.spec.ts,packages/**/*.spec.tsx,packages/**/*.test.ts,packages/**/*.test.tsx'

coverage_exclusions="$(ag -Ql '/* istanbul ignore file */' --ts | tr '\n' , | sed 's/,$//')"

lcov_report_paths="$(find packages -type f -name lcov.info -printf "%p," | sed 's/,$//')"

cat <<EOF > ./sonar-project.properties
sonar.projectKey=banking-libs
sonar.projectName=Banking Libs
sonar.scm.provider=git
sonar.sourceEncoding=UTF-8
sonar.organization=vizir-banking

sonar.sources=$sources
sonar.exclusions=$exclusions

sonar.tests=.
sonar.test.inclusions=$test_inclusions

sonar.coverage.exclusions=$coverage_exclusions
sonar.javascript.lcov.reportPaths=$lcov_report_paths
EOF

echo "Generated sonar-project.properties:"
cat ./sonar-project.properties
